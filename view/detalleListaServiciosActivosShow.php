<?php include('head.php'); ?>
<div class="container">
  <h2 class="text-center">Mis servicios</h2>
<br>
  <table class="table">
    <tr>
      <td><label>Id</label></td>
      <td><label>Tipo de Servicio</label></td>
      <td><label>Nombre</label> </td>
      <td><label>Descripción</label></td>
      <td><label>Precio</label></td>
      <td><label>Estado</label></td>
    </tr>
    <?php if (isset($all) && is_array($all)) { ?>
  <?php foreach ($all as $key => $value): ?>

<?php
    $estadoPeriodo= $value->status;
    $mostrarEstado = null;
    if($estadoPeriodo == 1){
      $mostrarEstado = "Inactivo";
    }else {
      $mostrarEstado= "Activo";
    }
    ?>

       <tr>
         <td><?php echo $value->id; ?></td>
         <td><?php echo $value->nombreServicio; ?></td>
         <td><?php echo $value->nombreDetalle; ?></td>
         <td><?php echo $value->descripcion; ?></td>
         <td><?php echo $value->precio; ?></td>
         <td><?php echo $mostrarEstado; ?></td>

         <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
           <td><a href="<?php echo $this->url("detalle","edit"); ?>&ID=<?php echo $value->ID; ?>" class="btn btn-primary">Editar</a></td>
         <?php endif; ?>
       </tr>
  <?php endforeach; ?>
<?php }else { ?>
  No hay servicios disponibles para mostrar.

  <?php } ?>
  </table>
  <br><br><br><br><br><br><br><br><br><br>
</div>

<?php include('footer.php'); ?>
