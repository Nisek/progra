<?php include('head.php'); ?>

    <h1 class="text-center">Cambio de contraseña del cliente.</h1>
    <hr/>
    <div class="container " >
      <div class="alert alert-info" role="alert">
  <label>¡NOTA!</label>
  <p>La contraseña debera de tener como minimo 8 caracteres y un maximo de 25.</p>
  </div>
      <form action="<?php echo $this->url("contrasenaCliente","insert"); ?>" method="post"  >


          <div class="form-group">
            <label>Contraseña actual:</label>
           <input type="password" name="contrasena" id="passActual" required class="form-control"/>
          </div>
          <div class="form-group">
            <label>Nueva contraseña:</label>
           <input type="password" minlength="8" maxlength="25" required pattern="[A-Za-z0-9]+" name="nuevoPass"required  class="form-control"
          title="La contrasena debe tener un mínimo de 8 caracteres conteniendo letras y números." id="nuevaContrasena" value=""/>
          </div>
          <div class="form-group">
            <label>Repita la nueva contraseña:</label>
           <input type="password" name="contrasenaValida" id="confirContrasena" required  class="form-control" minlength="8" maxlength="25"
           title="La contrasena debe tener un mínimo de 8 caracteres conteniendo letras y números."  required pattern="[A-Za-z0-9]+" onblur="comprobarClave()"/>
          </div>

          <input type="hidden" name="id" value="<?php echo $_SESSION['usuario']['id']; ?> " />
          <input type="hidden" name="id_roll" value="<?php echo $_SESSION['usuario']['id_roll']; ?> " />

          <input type="submit" value="Guardar" class="btn btn-success" />
      </form>
    </div>
<?php include('footer.php'); ?>


<script>
function comprobarClave(){
    clave1 = $("#nuevaContrasena").val();
    clave2 = $("#confirContrasena").val();

    if (clave1 != clave2){
       alert("La nueva contraseña y su confirmación deben coincidir.");
       document.getElementById("confirContrasena").value = "";
     }
}
</script>
