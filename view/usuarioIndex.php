<?php include('head.php'); ?>
<div class="container">
      <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
  <h1 class="text-center">Apartado de Usuarios y Clientes</h1>

  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><label>Usuarios.</label></h5>
          <p class="card-text">En este apartadose se pueden ingresar nuevos usuario y a su vez tambien se muestran.</p>
          <a href="<?php echo $this->url("usuario","add"); ?>" class="btn btn-primary">Agregar</a>
          <a href="<?php echo $this->url("usuario","show"); ?>" class="btn btn-primary">Mostrar</a>
        </div>
      </div>
    </div>
    <?php endif; ?>

    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title"><label>Clientes.</label></h5>
          <p class="card-text">En este apartado se ingresan los clientes al sistema y de igual manera se pueden mostrar.</p>
          <a href="<?php echo $this->url("cliente","add"); ?>" class="btn btn-primary">Agregar </a>
          <a href="<?php echo $this->url("cliente","show"); ?>" class="btn btn-primary">Mostrar</a>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include('footer.php'); ?>
