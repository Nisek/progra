<?php include('head.php'); ?>
<div class="container">
  <h1 class="text-center">Apartado de Servicios</h1>
<hr/>
<div >

<div class="col-sm-6">
  <div class="card">
    <div class="card-body">
      <h3 class="card-title"> <label>Disponibilidad de los servicios.</label></h3>
      <p class="card-text"><strong>PASO 1</strong> Se ingresa la cantidad de servicios que van a estar disponibles para los clientes.</p>
      <a href="<?php echo $this->url("detalle","add"); ?>" class="btn btn-primary">Agregar</a>
      <a href="<?php echo $this->url("detalle","show"); ?>" class="btn btn-primary">Mostrar</a>
    </div>
  </div>
</div>

  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h3 class="card-title"> <label>Asignación de servicios a clientes.</label></h3>
      <p class="card-text"> <strong>PASO 2</strong> En esa seccion se le asignan los servicios a los clientes.</p>

        <a href="<?php echo $this->url("clientexservicio","add"); ?>" class="btn btn-primary">Agregar</a>
        <a href="<?php echo $this->url("clientexservicio","show"); ?>" class="btn btn-primary">Mostrar</a>

      </div>
    </div>
  </div>
</div>

</div>
</div>


<br>
<br>
<br>
<br>


<?php include('footer.php'); ?>
