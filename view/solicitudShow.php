<?php include('head.php'); ?>
<div class="container">
  <h2 class="text-center">Lista de solicitudes disponibles.</h2>
<br>
  <table class="table">
    <tr>
      <td><label>Id</label></td>
      <td><label>Estado de la solicitud</label> </td>
      <td><label>Nombre</label></td>
      <td><label>Descripción</label></td>
      <td><label>Periodo</label></td>
    </tr>
    <?php if (isset($all)) { ?>
  <?php foreach ($all as $key => $value): ?>
    <?php
    $estadoSolicitud= $value->status;
    $mostrarEstado = null;
    if($estadoSolicitud == 1){
      $mostrarEstado = "Enviada";
    }else {
      $mostrarEstado= "Aprobada";
    }
    ?>
       <tr>
         <td><?php echo $value->id_detalle_servicio; ?></td>
         <td><?php echo $mostrarEstado; ?></td>
         <td><?php echo $value->nombre; ?></td>
         <td><?php echo $value->descripcion; ?></td>
         <td><?php echo $value->nombrePeriodo; ?></td>
         <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
           <td><a href="<?php echo $this->url("detalle","edit"); ?>&ID=<?php echo $value->ID; ?>" class="btn btn-primary">Editar</a></td>
         <?php endif; ?>
       </tr>
  <?php endforeach; ?>
  <?php } ?>
  </table>
  <br><br><br><br><br><br><br><br><br><br>
</div>


<?php include('footer.php'); ?>
