<?php include('head.php'); ?>
<div class="container">
  <h2 class="text-center">Agregar Periodos.</h2>


  <form  action="<?php echo $this->url("periodo","insert"); ?>" method="post">

    <div class="form-group">
      <label>Nombre:</label>
      <input type="text" name="nombre" required class="form-control" value=" "/>
    </div>

    <div class="form-group">
      <label>Fecha inicio:</label>
      <input type="date" name="fecha_inicio" max="3000-12-31"
       min="1000-01-01" class="form-control">
    </div>

    <div class="form-group">
      <label>Fecha fin:</label>
      <input type="date" name="fecha_fin" max="3000-12-31"
       min="1000-01-01" class="form-control">

    </div>
    <label>Estado:</label><br>
    <select name="statusSelect">
      <option value="0">Active</option>
      <option value="1">Inactive</option>
    </select>
    <br>
    <br>
      <input type="submit" value="Guardar" class="btn btn-success" />
  </form>


</div>

<?php include('footer.php'); ?>
