<?php include('head.php'); ?>

    <h1 class="text-center">Cambio de contraseña.</h1>
    <hr/>
    <div class="container " >
      <form action="<?php echo $this->url("contrasena","insert"); ?>" method="post"  >


          <div class="form-group">
          Contraseña actual: <input type="password" name="contrasena" id="passActual" required class="form-control" onblur="validarPassActual()"/>
          </div>
          <div class="form-group">
          Nueva contraseña: <input type="password" minlength="8" maxlength="25" required pattern="[A-Za-z0-9]+" name="nuevoPass"required  class="form-control"
          title="La contrasena debe tener un mínimo de 8 caracteres conteniendo letras y números." id="nuevaContrasena" value=""/>
          </div>
          <div class="form-group">
          Repita la nueva contraseña: <input type="password" name="contrasenaValida" id="confirContrasena" required  class="form-control" minlength="8" maxlength="25"
           title="La contrasena debe tener un mínimo de 8 caracteres conteniendo letras y números."  required pattern="[A-Za-z0-9]+" onblur="comprobarClave()"/>
          </div>

          <input type="hidden" name="id" value="<?php echo $_SESSION['usuario']['id']; ?> " />
          <input type="hidden" name="id_roll" value="<?php echo $_SESSION['usuario']['id_roll']; ?> " />

          <input type="submit" value="Guardar" class="btn btn-success" />
      </form>
    </div>
<?php include('footer.php'); ?>


<script>
function comprobarClave(){
    clave1 = $("#nuevaContrasena").val();
    clave2 = $("#confirContrasena").val();

    if (clave1 != clave2){
       alert("La nueva contraseña y su confirmación deben coincidir.");
       document.getElementById("confirContrasena").value = "";
     }
}
</script>

<script>
function validarPassActual()
{
  passActual= obtenerPassActual();
  passDigitado=  $("#passActual").val().trim();

  if (passActual != passDigitado){
     alert("La contraseña actual no es válida, verifique y vuelva a intentar.");
     document.getElementById("passActual").value = "";
   }
}
</script>

<script>
function obtenerPassActual(){
     nombre=  "<?php echo $_SESSION['usuario']['nombre']; ?> ";
     apellido=  "<?php echo $_SESSION['usuario']['apellido']; ?> ";
     cedula=  "<?php echo $_SESSION['usuario']['cedula']; ?> ";

     primerLetraNombre = nombre.substring(0,1);
     tresPrimApe = apellido.substring(0,3);
     cuatroUltCed = cedula.slice(-5);

  passActual = primerLetraNombre + ""+tresPrimApe +""+cuatroUltCed;

  return passActual.trim();
}


</script>
