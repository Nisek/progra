<?php
$roll = "";
?>
<nav class="mimenu">

      <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
        <nav class="navbar navbar-dark bg-dark" >
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("home","index"); ?>">Home</a>
              <?php if ($_SESSION['usuario']['reqCambio'] != 1): ?>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("usuario","index"); ?>">Usuarios</a>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("solicitudReconexion","index"); ?>">Solicitudes</a>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("servicio","index"); ?>">Servicios</a>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("actividad","index"); ?>">Historial</a>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("periodo","index"); ?>">Periodo</a>
              <?php endif; ?>
              <?php  $roll = "MASTER"; ?>
              <p class="navbar-brand">Master: <?php echo $_SESSION['usuario']['nombre']." ".$_SESSION['usuario']['apellido']; ?></p>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="login/salir.php">Cerrar Sesión</a>
              <?php endif; ?>
            </div>
          </div>
        </nav>


    <?php if ($_SESSION['usuario']['id_roll'] == 2): ?>
    <nav class="navbar navbar-dark bg-primary" >
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("home","index"); ?>">Home</a>
          <?php if ($_SESSION['usuario']['reqCambio'] != 1): ?>
            <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("solicitudReconexion","index"); ?>">Solicitudes</a>
            <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("periodo","index"); ?>">Periodo</a>
          <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("clientexservicio","index"); ?>">Servicios</a>
          <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("usuario","index"); ?>">Clientes</a>
<?php endif; ?>
              <?php  $roll = "ADMIN"; ?>
              <p class="navbar-brand">Admin: <?php echo $_SESSION['usuario']['nombre']." ".$_SESSION['usuario']['apellido']; ?></p>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="login/salir.php">Cerrar Sesión</a>
              <?php endif; ?>
        </div>
      </div>
    </nav>


    <?php if ($_SESSION['usuario']['id_roll'] == 3): ?>
    <nav class="navbar navbar-light" style="background-color:#4bf995  ;" >
      <div class="container-fluid">
        <div class="navbar-header">
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("home","index"); ?>">Home</a>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("pago","index"); ?>">Pagos</a>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("solicitud","index"); ?>">Solicitudes</a>
              <?php if ($_SESSION['usuario']['reqCambio'] != 1): ?>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("servicio","index"); ?>">Servicios</a>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="<?php echo $this->url("contrasenaCliente","Change"); ?>">Cambio de clave</a>
              <?php endif; ?>
              <?php  $roll = "CLIENTE"; ?>
              <p class="navbar-brand">Cliente: <?php echo $_SESSION['usuario']['nombre']." ".$_SESSION['usuario']['apellido']; ?></p>
              <a class="navbar-brand btn btn-sm btn-outline-secondary" type="button" href="login/salir.php">Cerrar Sesión</a>
              <?php endif; ?>
        </div>
      </div>
    </nav>

</nav>
