<?php include('head.php'); ?>

<div class="container">
  <h2 class="text-center">Lista periodos y servicios agregados.</h2>
<?php
if (isset($all) && is_array($all)){ ?>
  <hr/>

<table class="table">
<thead class="black white-text">
  <tr>
    <th scope="col">Estado del periodo</th>
    <th scope="col">Periodo</th>
    <th scope="col">Código del servicio</th>
    <th scope="col">Descripción</th>
<th scope="col">Total</th>
  </tr>
</thead>
<tbody>
  <tr>
    <?php
    foreach ($all as $key => $value): ?>

    <?php
    $estadoPeriodo= $value->status;
    $mostrarEstado = null;
    if($estadoPeriodo == 1){
      $mostrarEstado = "Concluido";
    }else {
      $mostrarEstado= "Activo";
    }
     ?>
         <tr>
           <td><?php echo $mostrarEstado; ?></td>
           <td><?php echo $value->nombrePeriodo; ?></td>
           <td><?php echo $value->descripcion; ?></td>
           <td><?php echo $value->nombre; ?></td>
           <td><?php echo $value->total; ?></td>

        
         </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
    <?php
    }else{ ?>
    No hay datos.
    <?php }?>
</div>

      <?php include('footer.php'); ?>
