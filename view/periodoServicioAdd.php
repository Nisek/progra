<?php include('head.php'); ?>
<div class="container">
  <h2 class="text-center">Agregar periodos a los servicios.</h2>
  <form action="<?php echo $this->url("periodoServicio","insert"); ?>" method="post"  >



      <div class="form-group">
        <?php if (isset($all ) && is_array($all )){ ?>

          <label>Codigo del Servicio:</label>

          <select name="id_detalle_servicio"  class="browser-default custom-select custom-select-lg mb-3" id="servicios" onChange="imprimirValor()">
            <?php foreach ($all as $key => $value): ?>

            <option value="<?php echo $value->ID; ?>,<?php echo $value->precio;?>"><?php echo $value->id_servicio." ".$value->nombre." ".$value->descripcion; ?></option>

            <?php endforeach; ?>
          </select>
            <?php
          }else{ ?>
           No hay datos.
          <?php }?>
      </div>
      <div class="form-group">
        <?php if (isset($periodosX ) && is_array($periodosX )){ ?>
            <label>Periodo:</label>
              <select name="id_periodo" id="periodos"  class="browser-default custom-select custom-select-lg mb-3">
              <?php foreach ($periodosX as $key => $value): ?>

              <option value="<?php echo $value->id; ?>"><?php echo $value->nombre; ?></option>

              <?php endforeach; ?>

            </select>
<?php
          }else{ ?>
           No hay datos.
          <?php }?>
      </div>
      <div class="form-group">
      Total: <input type="text" required name="total" id="total" class="form-control" readonly/>
      </div>

      <label>Estado del Servicio.</label>
      <select name="statusSelect"  class="browser-default custom-select custom-select-lg mb-3">
        <option value="0">Activo</option>
        <option value="1">Inactivo</option>
      </select>

      <input type="submit" value="Guardar" class="btn btn-success" />

  </form>
</div>

<?php include('footer.php'); ?>

<script>
window.onload = function() {
  imprimirValor();
}

function imprimirValor(){
  var select = document.getElementById("servicios");
  var miCadena = select.value;
  var divisiones = miCadena.split(",");
  document.getElementById("total").value = divisiones[1];
}
</script>
