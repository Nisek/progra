<?php include('head.php'); ?>
<div class="container">
  <h1 class="text-center">Registro de actividad de los usuarios</h1>
  <hr/>
  <a name="arriba"></a>
  <a href="#abajo">Ir a la parte de abajo</a>

  <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
  <table class="table">
    <tr>
      <td>Id</td>
      <td>Descripción</td>
      <td>Tabla</td>
      <td>Acción</td>
      <td>Fecha</td>
      <td>Id del usuario</td>
    </tr>
  <?php foreach ($all as $key => $value): ?>
       <tr>
        <?php $desc= $value->descripcion; ?>
        <?php $descDeco= decodificar($desc); ?>
         <td><?php echo $value->id; ?></td>
         <td><?php echo $descDeco; ?></td>
         <td><?php echo $value->tabla; ?></td>
         <td><?php echo $value->accion; ?></td>
         <td><?php echo $value->fecha; ?></td>
         <td><?php echo $value->id_usuario; ?></td>
       </tr>
  <?php endforeach; ?>
  </table>
  <a href="#arriba">Ir a la parte de arriba</a>
  <a name="abajo"></a>
</div>

<?php
else: ?>
  <h3 class="text-center">Sección exclusiva del usuario master</h3>
<?php endif; ?>


<?php include('footer.php'); ?>



<?php
function decodificar($dato) {
         $resultado = base64_decode($dato);
         return $resultado;
     }
     ?>
