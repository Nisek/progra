<?php include('head.php'); ?>
<div class="container">
    <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
  <h1>Agregar Usuario</h1>
  <hr/>
  <div class="alert alert-dark" role="alert">
    <label>¡NOTA!</label>
    <p>La contraseña se genera de manera automatica, la cual será, la primer letra del nombre, seguido de las tres primeras letras del apellido y por ultimo los cuatro digitos finales de la cédula
    , el cual se genera de manera automatica y deberá de cambiar en el primer ingreso.</p>

  </div>
  <div >
    <form action="<?php echo $this->url("usuario","insert"); ?>" method="post"  >


      <?php// var_dump($rolls); ?>
        <div class="form-group">
         <?php if (isset($rolls) && is_array($rolls)) { ?>
            Roll: <select name="id_roll" name="">

              <option value="1"> MASTER </option>
              <option value="2"> ADMIN </option>

          </select>
        <?php }else { ?>
          No hay datos.

          <?php } ?>
        <!-- Roll: <input type="text" name="id_roll" class="form-control" value=""/> -->
        </div>
        <div class="form-group">
        Nombre: <input type="text" name="nombre"required class="form-control" value=""/>
        </div>
        <div class="form-group">
        Apellido: <input type="text" name="apellido"required class="form-control" value=""/>
        </div>
        <div class="form-group">
        Cédula: <input type="text" name="cedula"required class="form-control" value=""/>
        </div>
        <div class="form-group">
        NickName: <input type="text" name="nickname" required class="form-control" value=""/>
        </div>



        <input type="submit" value="enviar" class="btn btn-success" />
    </form>
  </div>
  <?php
  else: ?>
    <h3 class="text-center">Sección exclusiva del usuario master</h3>
  <?php endif; ?>
</div>

<?php include('footer.php'); ?>
