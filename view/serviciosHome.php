<?php include('head.php'); ?>
<div class="container">
  <h1>Apartado de Servicios</h1>

  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Agregar Usuarios.</h5>
          <p class="card-text">En este apartado de deben de ingrear los nuevos usuarios.</p>
          <a href="<?php echo $this->url("servicio", "add"); ?>" class="btn btn-primary">Agregar</a>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Mostrar Usuarios.</h5>
          <p class="card-text">En este apartado se muestra la lista de usuarios registrados en el sistema.</p>
          <a href="<?php echo $this->url("servicio", "show"); ?>" class="btn btn-primary">Mostrar</a>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include('footer.php'); ?>
