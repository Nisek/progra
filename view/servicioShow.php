<?php include('head.php'); ?>
<div class="container">
  <h1>Mostrar Servicios</h1>
  <hr/>
  <table class="table table-default">
    <tr>
      <td>Id</td>
      <td>Nombre</td>
      <td>Descripción</td>
      <td>Estatus</td>
    </tr>
    <?php if (isset($all)) { ?>
  <?php foreach ($all as $key => $value): ?>
    <?php
    $estadoServicio= $value->status;
    $estado = null;
    if($estadoServicio == 1){
      $mostrarEstado = "Inactivo";
    }else {
      $mostrarEstado= "Activo";
    }
    ?>
       <tr>
         <td><?php echo $value->id; ?></td>
         <td><?php echo $value->nombre; ?></td>
         <td><?php echo $value->descripcion; ?></td>
         <td><?php echo $mostrarEstado; ?></td>
         <!--<td><a href="<?php echo $this->url("servicio","erase"); ?>&id=<?php echo $value->id; ?>" class="btn btn-danger">Borrar</a></td>-->
         <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
           <td><a href="<?php echo $this->url("servicio","edit"); ?>&id=<?php echo $value->id; ?>" class="btn btn-primary">Editar</a></td>
         <?php endif; ?>
       </tr>
  <?php endforeach; ?>
  <?php } ?>
  </table>
</div>

<?php include('footer.php'); ?>
