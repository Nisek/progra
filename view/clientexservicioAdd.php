<?php include('head.php'); ?>


    <div class="container " >
      <h2 class="text-center">Modificar Servicios de los Clientes</h2>
      <hr/>
      <form action="<?php echo $this->url("clientexservicio","insert"); ?>" method="post"  >
          <div class="form-group">
            <?php if (isset($clientesg ) && is_array($clientesg )){ ?>

              <label>Nombre del Cliente:</label>

              <select name="IdCliente"  class="browser-default custom-select custom-select-lg mb-3">
                <?php foreach ($clientesg as $key => $value): ?>

                <option value="<?php echo $value->id; ?>"><?php echo $value->nombre." ".$value->apellido." ".$value->cedula; ?></option>

                <?php endforeach; ?>
              </select>
                <?php
              }else{ ?>
               no hay datos.
              <?php }?>
          </div>
          <div class="form-group">
            <?php if (isset($serviciosg ) && is_array($serviciosg )){ ?>
                <label>Nombre del Servicio:</label>
                  <select name="IdDetalleServicio"  class="browser-default custom-select custom-select-lg mb-3">
                  <?php foreach ($serviciosg as $key => $value): ?>

                  <option value="<?php echo $value->ID; ?>"><?php echo $value->nombre; ?></option>

                  <?php endforeach; ?>

                </select>
<?php
              }else{ ?>
               no hay datos.
              <?php }?>
          </div>
          <label>Estado del Servicio.</label>
          <select name="statusSelect"  class="browser-default custom-select custom-select-lg mb-3">
            <option value="0">Activo</option>
            <option value="1">Inactivo</option>
          </select>

          <input type="submit" value="Guardar" class="btn btn-success" />

      </form>
    </div>
<?php include('footer.php'); ?>
