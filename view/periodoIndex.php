<?php include('head.php'); ?>
<div class="container">

  <h2 class="text-center">Periodos</h2>
  <hr/>

<div class="">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h3 class="card-title"> <label>Ingreso de nuevos periodos</label></h3>
        <p class="card-text"> <strong>PASO 1</strong> En esta sección puede agregar los periodos de tiempo al detalle de los servicios.</p>
        <a href="<?php echo $this->url("periodo", "add"); ?>" class="btn btn-primary">Agregar</a>
        <a href="<?php echo $this->url("periodo", "show"); ?>" class="btn btn-primary ">Mostrar</a>
      </div>
    </div>
  </div>

</div>

<br>
</div>
</div>


<?php include('footer.php'); ?>
