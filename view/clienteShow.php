<?php include('head.php'); ?>
<div class="container">
  <h1 class="text-center">Lista de clientes</h1>
  <hr/>
  <a name="arriba"></a>
  <a href="#abajo">Ir a la parte de abajo</a>
  <table class="table">
    <tr>
      <td>Id</td>
      <td>Nombre</td>
      <td>Apellido</td>
      <td>Cédula</td>
      <td>Status</td>
      <td>Nick Name</td>


    </tr>
    <?php if (isset($all)) { ?>
  <?php foreach ($all as $key => $value): ?>
    <?php
    $estadoCliente= $value->status;
    $estado = null;
    if($estadoCliente == 1){
      $mostrarEstado = "Inactivo";
    }else {
      $mostrarEstado= "Activo";
    }
    ?>

       <tr>
         <td><?php echo $value->id; ?></td>
         <td><?php echo $value->nombre; ?></td>
         <td><?php echo $value->apellido; ?></td>
         <td><?php echo $value->cedula; ?></td>
         <td><?php echo $mostrarEstado; ?></td>
         <td><?php echo $value->nickname; ?></td>
         <!--<td><a href="<?php echo $this->url("cliente","erase"); ?>&id=<?php echo $value->id; ?>" class="btn btn-danger">Borrar</a></td>-->
         <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
           <td><a href="<?php echo $this->url("cliente","edit"); ?>&id=<?php echo $value->id; ?>" class="btn btn-primary">Modificar</a></td>
         <?php endif; ?>
       </tr>
  <?php endforeach; ?>
  <?php } ?>
  </table>
  <a href="#arriba">Ir a la parte de arriba</a>
  <a name="abajo"></a>
</div>

<?php include('footer.php'); ?>
