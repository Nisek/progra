<?php include('head.php'); ?>
<div class="container">
  <h2 class="text-center">Lista de servicios disponibles.</h2>
<br>
  <table class="table">
    <tr>
      <td><label>ID</label></td>
      <td><label>ID Servicio</label> </td>
      <td><label>Nombre</label></td>
      <td><label>Código</label></td>
      <td><label>Precio</label></td>
      <td><label>Status</label></td>
    </tr>
    <?php if (isset($all)) { ?>
  <?php foreach ($all as $key => $value): ?>

    <?php
    $estadoServicio= $value->status;
    $estado = null;
    if($estadoServicio == 1){
      $mostrarEstado = "Inactivo";
    }else {
      $mostrarEstado= "Activo";
    }
    ?>
       <tr>
         <td><?php echo $value->ID; ?></td>
         <td><?php echo $value->id_servicio; ?></td>
         <td><?php echo $value->nombre; ?></td>
         <td><?php echo $value->descripcion; ?></td>
         <td><?php echo $value->precio; ?></td>
         <td><?php echo $mostrarEstado; ?></td>
         <!--<td><a href="<?php echo $this->url("detalle","erase"); ?>&id=<?php echo $value->ID; ?>" class="btn btn-danger">Borrar</a></td>-->
         <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
           <td><a href="<?php echo $this->url("detalle","edit"); ?>&ID=<?php echo $value->ID; ?>" class="btn btn-primary">Editar</a></td>
         <?php endif; ?>
       </tr>
  <?php endforeach; ?>
  <?php } ?>
  </table>
  <br><br><br><br><br><br><br><br><br><br>
</div>

<?php include('footer.php'); ?>
