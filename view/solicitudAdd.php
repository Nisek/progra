<?php include('head.php'); ?>
<div class="container">
  <h1>Agregar solicitud de reconexión.</h1>
  <hr/>
  <div  >
    <form action="<?php echo $this->url("solicitud","insert"); ?>" method="post"  >

      <div class="form-group">
        <?php if (isset($all ) && is_array($all )){ ?>

          <label>Codigo del Servicio:</label>

          <select name="id_detalle_servicio"  class="browser-default custom-select custom-select-lg mb-3" id="servicios" onChange="cargarDatos()">
            <?php foreach ($all as $key => $value): ?>

            <?php
            $yaPago = $value->pago;
            if($yaPago == 0)
            {
              $mostrarEstado= "Pagado";
            }
            elseif($yaPago == 1) {
              $mostrarEstado= "Sin pagar";
            }


             ?>
            <option value="<?php echo $value->idDelCxDS; ?>,<?php echo $value->nombrePeriodo; ?>,
              <?php echo $value->id_periodo; ?>,<?php echo $value->id_detalle_servicio; ?>, <?php echo $mostrarEstado;  ?>">
              <?php echo $value->nombreDS." - ".$value->descripcion; ?></option>

            <?php endforeach; ?>
          </select>
            <?php
          }else{ ?>
           No hay servicios en estado desconectado.
          <?php }?>
      </div>

      <div class="form-group">
      Periodo:<input type="text" required name="periodo" id="periodo" class="form-control" readonly/>
      </div>
      <div class="form-group">
      Estado de pago:<input type="text" required name="pago" id="pago" class="form-control"
      title="Debe realizar el pago del servicio para solicitar reconexión." readonly/>
      </div>

      <input type="text" style="display:none;" name="idDelPeriodo" id="idDelPeriodo"/>
      <input type="text" style="display:none;" name="id_detalle_servicio" id="id_detalle_servicio"/>

        <br>
        <br>

        <input type="submit" id="botonReconexion" value="Solicitar reconexión" class="btn btn-success" />
        <span class="label label-danger" id="error" style="display:none">Debe realizar el pago del servicio para solicitar reconexión.</span>
    </form>
  </div>
</div>
<?php include('footer.php'); ?>

<script>
window.onload = function() {
  cargarDatos();
}

function cargarDatos(){
  var select = document.getElementById("servicios");
  var miCadena = select.value;
  var divisiones = miCadena.split(",");
  document.getElementById("periodo").value = divisiones[1];
  document.getElementById("idDelPeriodo").value = divisiones[2];
  document.getElementById("id_detalle_servicio").value = divisiones[3];
  document.getElementById("pago").value = divisiones[4];

  verificarPago();
}

function verificarPago()
{
  var pago= document.getElementById("pago").value;
  if(pago.trim() == "Sin pagar")
  {
    document.getElementById("botonReconexion").disabled=true;
    document.getElementById('error').style.display = 'inline';
  }
  else {
    document.getElementById("botonReconexion").disabled=false;
    document.getElementById('error').style.display = 'none';
  }
}
</script>
