<?php include('head.php'); ?>
    <h1 class="text-center">Registro de clientes.</h1>
    <hr/>
    <div class="container " >
      <div class="alert alert-dark" role="alert">
        <label>¡NOTA!</label>
        <p>La contraseña se genera de manera automática, la cual será: la primer letra del nombre, seguido de las tres primeras letras del apellido y por último los cuatro dígitos finales de la cédula,
          esta se genera de manera automática y deberá ser cambiada en el primer ingreso.</p>

      </div>
      <form action="<?php echo $this->url("cliente","insert"); ?>" method="post"  >


          <div class="form-group">
          Nombre: <input type="text" name="nombre" required class="form-control" value=""/>
          </div>
          <div class="form-group">
          Apellido: <input type="text" name="apellido"required  class="form-control" value=""/>
          </div>
          <div class="form-group">
          Cédula: <input type="text" name="cedula"required  class="form-control" value=""/>
          </div>
          <div class="form-group">
          Nick Name: <input type="text" name="nickname" required class="form-control" value=""/>
          </div>



          <select name="statusSelect" name="">
            <option value="0">Active</option>s
            <option value="1">Inactive</option>
          </select>
<br><br>
          <input type="submit" value="Guardar" class="btn btn-success" />
      </form>
    </div>
<?php include('footer.php'); ?>
