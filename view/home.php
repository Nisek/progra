<?php include('head.php'); ?>
<!--Section: Live preview-->
  <?php if ($_SESSION['usuario']['reqCambio'] == 1): ?>
    <div class="container">
      <h1>¡Cambio de contraseña!</h1>
      <h5 class="text-center">Debe realizar el cambio de contraseña para continuar con el uso del sistema.</h5>
      <a href="<?php echo $this->url("contrasena","Index"); ?>" class="btn btn-danger">Cambiar Contraseña</a>

    </div>

    <?php
    else: ?>
<section id="v-1">

  <!-- Section: Magazine v.1 -->
  <section class="magazine-section my-5">

    <!-- Section heading -->
    <h2 class="h1-responsive font-weight-bold text-center my-5">Endesa en el Mundo</h2>
    <!-- Section description -->
    <p class="text-center dark-grey-text w-responsive mx-auto mb-5">Nuestros servicios se encuentran en gran parte del mundo, brindando
      calidad en nuestros productos con tecnologia de punta.</p>

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-lg-6 col-md-12">

        <!-- Featured news -->
        <div class="single-news mb-4">

          <!-- Image -->
          <div class="view overlay rounded z-depth-1-half mb-4">
            <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/82.jpg" alt="Sample image">
            <a>
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>

          <!-- Data -->
          <div class="news-data d-flex justify-content-between">
            <a href="#!" class="light-blue-text">
              <h6 class="font-weight-bold"><i class="fas fa-plane pr-2"></i>Travels</h6>
            </a>
            <p class="font-weight-bold dark-grey-text"><i class="fas fa-clock-o pr-2"></i>20/08/2018</p>
          </div>

          <!-- Excerpt -->
          <h3 class="font-weight-bold dark-grey-text mb-3"><a>Title of the news</a></h3>
          <p class="dark-grey-text">Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil
            impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis
            dolor
            repellendus.</p>

        </div>
        <!-- Featured news -->

        <!-- Small news -->
        <div class="single-news mb-4">

          <!-- Grid row -->
          <div class="row">

            <!-- Grid column -->
            <div class="col-md-3">

              <!--Image-->
              <div class="view overlay rounded z-depth-1 mb-4">
                <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/photo8.jpg" alt="Sample image">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-9">

              <!-- Excerpt -->
              <p class="font-weight-bold dark-grey-text">19/08/2018</p>
              <div class="d-flex justify-content-between">
                <div class="col-11 text-truncate pl-0 mb-3">
                  <a href="#!" class="dark-grey-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a>
                </div>
                <a><i class="fas fa-angle-double-right"></i></a>
              </div>

            </div>
            <!-- Grid column -->

          </div>
          <!-- Grid row -->

        </div>
        <!-- Small news -->

        <!-- Small news -->
        <div class="single-news mb-4">

          <!-- Grid row -->
          <div class="row">

            <!-- Grid column -->
            <div class="col-md-3">

              <!--Image-->
              <div class="view overlay rounded z-depth-1 mb-4">
                <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/54.jpg" alt="Sample image">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-9">

              <!-- Excerpt -->
              <p class="font-weight-bold dark-grey-text">18/08/2018</p>
              <div class="d-flex justify-content-between">
                <div class="col-11 text-truncate pl-0 mb-3">
                  <a href="#!" class="dark-grey-text">Soluta nobis est eligendi optio cumque nihil impedit
                    quo
                    minus</a>
                </div>
                <a><i class="fas fa-angle-double-right"></i></a>
              </div>

            </div>
            <!-- Grid column -->

          </div>
          <!-- Grid row -->

        </div>
        <!-- Small news -->

        <!-- Small news -->
        <div class="single-news mb-lg-0 mb-4">

          <!-- Grid row -->
          <div class="row">

            <!-- Grid column -->
            <div class="col-md-3">

              <!--Image-->
              <div class="view overlay rounded z-depth-1 mb-lg-0 mb-4">
                <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" alt="Sample image">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-9">

              <!-- Excerpt -->
              <p class="font-weight-bold dark-grey-text">17/08/2018</p>
              <div class="d-flex justify-content-between">
                <div class="col-11 text-truncate pl-0 mb-lg-0 mb-3">
                  <a href="#!" class="dark-grey-text">Voluptatem accusantium doloremque</a>
                </div>
                <a><i class="fas fa-angle-double-right"></i></a>
              </div>

            </div>
            <!-- Grid column -->

          </div>
          <!-- Grid row -->

        </div>
        <!-- Small news -->

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-lg-6 col-md-12">

        <!-- Featured news -->
        <div class="single-news mb-4">

          <!-- Image -->
          <div class="view overlay rounded z-depth-1-half mb-4">
            <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/84.jpg" alt="Sample image">
            <a>
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>

          <!-- Data -->
          <div class="news-data d-flex justify-content-between">
            <a href="#!" class="pink-text">
              <h6 class="font-weight-bold"><i class="fas fa-home pr-2"></i>Lifestyle</h6>
            </a>
            <p class="font-weight-bold dark-grey-text"><i class="fas fa-clock-o pr-2"></i>24/08/2018</p>
          </div>

          <!-- Excerpt -->
          <h3 class="font-weight-bold dark-grey-text mb-3"><a>Noticias Recientes</a></h3>
          <p class="dark-grey-text">Sed ut perspiciatis unde voluptatem omnis iste natus error sit voluptatem
            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
            et quasi architecto beatae.</p>

        </div>
        <!-- Featured news -->

        <!-- Small news -->
        <div class="single-news mb-4">

          <!-- Grid row -->
          <div class="row">

            <!-- Grid column -->
            <div class="col-md-3">

              <!--Image-->
              <div class="view overlay rounded z-depth-1 mb-4">
                <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/86.jpg" alt="Sample image">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-9">

              <!-- Excerpt -->
              <p class="font-weight-bold dark-grey-text">23/08/2018</p>
              <div class="d-flex justify-content-between">
                <div class="col-11 text-truncate pl-0 mb-3">
                  <a href="#!" class="dark-grey-text">Itaque earum rerum hic tenetur a sapiente delectus</a>
                </div>
                <a><i class="fas fa-angle-double-right"></i></a>
              </div>

            </div>
            <!-- Grid column -->

          </div>
          <!-- Grid row -->

        </div>
        <!-- Small news -->

        <!-- Small news -->
        <div class="single-news mb-4">

          <!-- Grid row -->
          <div class="row">

            <!-- Grid column -->
            <div class="col-md-3">

              <!--Image-->
              <div class="view overlay rounded z-depth-1 mb-4">
                <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/48.jpg" alt="Sample image">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-9">

              <!-- Excerpt -->
              <p class="font-weight-bold dark-grey-text">22/08/2018</p>
              <div class="d-flex justify-content-between">
                <div class="col-11 text-truncate pl-0 mb-3">
                  <a href="#!" class="dark-grey-text">Soluta nobis est eligendi optio cumque nihil impedit
                    quo
                    minus</a>
                </div>
                <a><i class="fas fa-angle-double-right"></i></a>
              </div>

            </div>
            <!-- Grid column -->

          </div>
          <!-- Grid row -->

        </div>
        <!-- Small news -->

        <!-- Small news -->
        <div class="single-news">

          <!-- Grid row -->
          <div class="row">

            <!-- Grid column -->
            <div class="col-md-3">

              <!--Image-->
              <div class="view overlay rounded z-depth-1 mb-md-0 mb-4">
                <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img%20(56).jpg"
                  alt="Sample image">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-9">

              <!-- Excerpt -->
              <p class="font-weight-bold dark-grey-text">21/08/2018</p>
              <div class="d-flex justify-content-between">
                <div class="col-11 text-truncate pl-0">
                  <a href="#!" class="dark-grey-text">Maiores alias consequatur aut perferendis</a>
                </div>
                <a><i class="fas fa-angle-double-right"></i></a>
              </div>

            </div>
            <!-- Grid column -->

          </div>
          <!-- Grid row -->

        </div>
        <!-- Small news -->

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </section>
  <!-- Section: Magazine v.1 -->
<?php endif; ?>

<?php include('footer.php'); ?>
