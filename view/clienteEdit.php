<?php include('head.php'); ?>
    <h1 class="text-center">Editar información de los clientes.</h1>
    <hr/>
    <div class="container " >
      <form action="<?php echo $this->url("cliente","update"); ?>" method="post"  >

          <input type="hidden" name="id" value="<?php echo $all[0]->id; ?> " />

          <div class="form-group">
          Nombre: <input type="text" name="nombre" required class="form-control" value="<?php echo $all[0]->nombre; ?> "/>
          </div>
          <div class="form-group">
          Apellido: <input type="text"required  name="apellido" class="form-control" value="<?php echo $all[0]->apellido; ?> "/>
          </div>
          <div class="form-group">
          Pass: <input type="password" name="pass" minlength="8" maxlength="25" required pattern="[A-Za-z0-9]+" class="form-control"
          title="La contrasena debe tener un mínimo de 8 caracteres conteniendo letras y números." value="<?php echo $all[0]->pass; ?>"/>
          </div>
          <div class="form-group">
          Cédula: <input type="text"required  name="cedula" class="form-control" value="<?php echo $all[0]->cedula; ?> "/>
          </div>
          <input type="hidden" name="id_roll" value="<?php echo $all[0]->id_roll; ?> " />
          <div class="form-group">
         <input type="text " hidden   name="logueado" class="form-control" value="<?php echo $all[0]->logueado; ?> "/>
          </div>
          <div class="form-group">
          Nick Name: <input type="text"required  name="nickname" class="form-control" value="<?php echo $all[0]->nickname; ?> "/>
          </div>

          <select name="statusSelect">
            <option value="0">Active</option>
            <option value="1">Inactive</option>
          </select>
          <br><br>
          <input type="hidden" name="id" value="<?php echo $all[0]->id; ?> " />
          <input type="submit" value="Guardar" class="btn btn-success" />

      </form>
    </div>
<?php include('footer.php'); ?>
