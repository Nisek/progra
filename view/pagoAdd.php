<?php include('head.php'); ?>
<h1 class="text-center">Efectuar pagos</h1>
<hr/>
<div class="container " >

  <form action="<?php echo $this->url("pago","insert"); ?>" method="post"  >

    <div class="form-group">
      <?php if (isset($all ) && is_array($all )){ ?>

        Código del Servicio:<select name="id_Para_Pagar"  class="browser-default custom-select custom-select-lg mb-3" id="servicios" onChange="cargarDatos()">
          <?php foreach ($all as $key => $value): ?>

          <option value="<?php echo $value->IdDSxPParaPago; ?>,<?php echo $value->nombrePeriodo; ?>,
            <?php echo $value->precio; ?>,<?php echo $value->total; ?>">
            <?php echo $value->nombreDS." - ".$value->descripcion; ?></option>

          <?php endforeach; ?>
        </select>
          <?php
        }else{ ?>
         No hay servicios en estado desconectado.
        <?php }?>
    </div>
    <div class="form-group">
    Periodo:<input type="text" required name="periodo" id="periodo" class="form-control" readonly/>
    </div>
    <div class="form-group">
    Precio:<input type="text" required name="precio" id="precio" class="form-control" readonly/>
    </div>
    <div class="form-group">
    Multa:<input type="text" required name="multa" id="multa" class="form-control" readonly/>
    </div>
    <div class="form-group">
    Total:<input type="text" required name="total" id="total" class="form-control" readonly/>
    </div>
  <input type="text" style="display:none;" name="id_Para_Pagar" id="id_Para_Pagar"/>
      <input type="submit" value="Efectuar pago" class="btn btn-success" />
  </form>
</div>

<?php include('footer.php'); ?>

<script>
window.onload = function() {
  cargarDatos();
}

function cargarDatos(){
  var select = document.getElementById("servicios");
  var miCadena = select.value;
  var divisiones = miCadena.split(",");
  var precio = divisiones[2].trim();
  var multa = divisiones[3].trim();
  document.getElementById("id_Para_Pagar").value = divisiones[0];
  document.getElementById("periodo").value = divisiones[1];
  document.getElementById("precio").value = precio;
  document.getElementById("multa").value = multa;

  var totalAPagar= calcularTotal(precio,multa);
  document.getElementById("total").value =  totalAPagar;

}

function calcularTotal(precio,multa)
{
  var total= parseInt(precio)+parseInt(multa);
  return total;
}
</script>
