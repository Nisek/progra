<?php include('head.php'); ?>

<div class="container">
  <h2>Lista de Clientes con sus Servicios.</h2>
<?php //echo "<pre>";var_dump($all);echo "<pre>";
if (isset($all) && is_array($all)){ ?>
  <hr/>

<table class="table">
<thead class="black white-text">
  <tr>
    <th scope="col">Id</th>
    <th scope="col">Cliente</th>
    <th scope="col">Detalle Servicio</th>
    <th scope="col">Status</th>
  </tr>
</thead>
<tbody>
  <tr>
    <?php
    foreach ($all as $key => $value): ?>
    <?php
    $estado= $value->status;
    $mostrarEstado = null;
    if($estado == 1){
      $mostrarEstado = "Inactivo";
    }else {
      $mostrarEstado= "Activo";
    }
     ?>
         <tr>
           <td><?php echo $value->id; ?></td>
           <td><?php echo $value->cnombre; ?></td>
           <td><?php echo $value->dsnombre; ?></td>
           <td><?php echo $mostrarEstado; ?></td>
           <!--<td><a href="<?php echo $this->url("servicio","erase"); ?>&id=<?php echo $value->id; ?>" class="btn btn-danger">Borrar</a></td>-->
           <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
             <td><a href="<?php echo $this->url("clientexservicio","edit"); ?>&id=<?php echo $value->id; ?>" class="btn btn-primary">Editar</a></td>
           <?php endif; ?>
         </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
    <?php
    }else{ ?>
    No hay servicios asignados a clientes.
    <?php }?>
</div>

      <?php include('footer.php'); ?>
