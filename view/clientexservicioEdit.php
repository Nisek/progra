<?php include('head.php'); ?>

<h1>Editar Cliente Servicio</h1>
<hr/>
<div class="container">
  <form action="<?php echo $this->url("clientexservicio","update"); ?>" method="post">

    <div class="form-group">
      <label>Nombre: </label>
      <input type="text" name="nombre" value="<?php echo $all[0]->cnombre; ?>" readonly class="form-control">
    </div>

    <div class="form-group">
      <label>Servicio: </label>
      <input type="text" name="servicio" value="<?php echo $all[0]->dsnombre; ?>" readonly class="form-control">
    </div>

    <select name="statusSelect" name="">
      <option value="0">Activo</option>
      <option value="1">Inactivo</option>
    </select>

<br><br>
      <input type="hidden" name="id" value="<?php echo $all[0]->id; ?>">
      <input type="submit" value="Guardar" class="btn btn-success" />


  </form>
</div>
<?php include('footer.php'); ?>
