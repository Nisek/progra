<?php include('head.php'); ?>
<div class="container">
  <h1 class="text-center">Lista de periodos.</h1>


<table class="table table-default">
  <tr>
    <td><label>Nombre</label></td>
    <td><label>Fecha Inicio</label></td>
    <td><label>Fecha Fin</label></td>
    <td><label>Estatus</label></td>
  </tr>
  <?php if (isset($all)) { ?>
  <?php foreach ($all as $key => $value): ?>
    <?php
    $estadoPeriodo= $value->status;
    if($estadoPeriodo == 1){
      $mostrarEstado = "Inactivo";
    }else {
      $mostrarEstado= "Activo";
    }
    ?>

    <tr>

      <td><?php echo $value->nombre; ?></td>
      <td><?php echo $value->fecha_inicio; ?></td>
      <td><?php echo $value->fecha_fin; ?></td>
      <td><?php echo $mostrarEstado; ?></td>
      <!--<td><a href="<?php echo $this->url("periodo","erase"); ?>&id=<?php echo $value->id; ?>" class="btn btn-danger">Borrar</a></td>-->
      <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
        <td><a href="<?php echo $this->url("periodo","edit"); ?>&id=<?php echo $value->id; ?>" class="btn btn-primary">Editar</a></td>
      <?php endif; ?>
    </tr>
<?php endforeach; ?>
<?php } ?>
</table>
</table>
</div>
<?php include('footer.php'); ?>
