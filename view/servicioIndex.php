<?php include('head.php'); ?>
<div class="container">
  <h1 class="text-center">Apartado de Servicios</h1>
<hr/>
  <?php if ($_SESSION['usuario']['id_roll'] != 3): ?>
<div >

  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h3 class="card-title"> <label>Ingreso de nuevos Servicios.</label></h3>
        <p class="card-text"> <strong>PASO 1</strong> En esa seccion se ingresan los servicios nuevos al sistema.</p>
        <a href="<?php echo $this->url("servicio", "add"); ?>" class="btn btn-primary">Agregar</a>
        <a href="<?php echo $this->url("servicio", "show"); ?>" class="btn btn-primary ">Mostrar</a>
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h3 class="card-title"> <label>Periodos</label></h3>
        <p class="card-text"><strong>PASO 3</strong> Se le agregan los periodos de tiempo a los servicios.</p>
        <a href="<?php echo $this->url("periodoServicio","add"); ?>" class="btn btn-primary">Agregar</a>
        <a href="<?php echo $this->url("periodoServicio","show"); ?>" class="btn btn-primary">Mostrar</a>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-6">
  <div class="card">
    <div class="card-body">
      <h3 class="card-title"> <label>Disponibilidad de los servicios.</label></h3>
      <p class="card-text"><strong>PASO 2</strong> Se ingresa la cantidad de servicios que van a estar disponibles para los clientes.</p>
      <a href="<?php echo $this->url("detalle","add"); ?>" class="btn btn-primary">Agregar</a>
      <a href="<?php echo $this->url("detalle","show"); ?>" class="btn btn-primary">Mostrar</a>
    </div>
  </div>
</div>

<div class="col-sm-6">
  <div class="card">
    <div class="card-body">
      <h3 class="card-title"> <label>Asignación de servicios a clientes.</label></h3>
    <p class="card-text"> <strong>PASO 4</strong> En esa seccion se le asignan los servicios a los clientes, los cuales ya cuentan con disponibilidad y sus respectivos periodos.</p>

      <a href="<?php echo $this->url("clientexservicio","add"); ?>" class="btn btn-primary">Agregar</a>
      <a href="<?php echo $this->url("clientexservicio","show"); ?>" class="btn btn-primary">Mostrar</a>

    </div>
  </div>
</div>




<?php
else: ?>
<div class="col-sm-6">
  <div class="card">
    <div class="card-body">
      <h3 class="card-title"> <label>Mis servicios.</label></h3>
      <p class="card-text"><strong>PASO 1</strong> Se muestra la lista de servicios del usuario.</p>
      <a href="<?php echo $this->url("detalle","showMisServicios"); ?>" class="btn btn-primary">Mostrar Pendientes</a>
      <a href="<?php echo $this->url("detalle","showMisServiciosActivos"); ?>" class="btn btn-primary">Mostrar Todos</a>
    </div>
  </div>
</div>
<?php endif; ?>

</div>
</div>


<br>
<br>
<br>
<br>


<?php include('footer.php'); ?>
