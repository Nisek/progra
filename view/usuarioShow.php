<?php include('head.php'); ?>
<div class="container">
      <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
  <h1 class="text-center">Mostrar Usuario</h1>
  <hr/>
  <table  class="table table-bordered">
    <tr>
      <td>id</td>

      <td>nombre</td>
      <td>apellido</td>
      <td>cedula</td>
      <td>nickname</td>
      <td>Status</td>
      <td>Modificar</td>
    </tr>
  <?php foreach ($all as $key => $value): ?>
    <?php
    $estadoUsuario= $value->status;
    $estado = null;
    if($estadoUsuario == 1){
      $mostrarEstado = "Inactivo";
    }else {
      $mostrarEstado= "Activo";
    }
    ?>
       <tr>
         <td><?php echo $value->id; ?></td>
         <td><?php echo $value->nombre; ?></td>
         <td><?php echo $value->apellido; ?></td>
         <td><?php echo $value->cedula; ?></td>
         <td><?php echo $value->nickname; ?></td>
         <td><?php echo $mostrarEstado; ?></td>
         <!--<td><a href="<?php echo $this->url("usuario","erase"); ?>&id=<?php echo $value->id; ?>" class="btn btn-danger">Borrar</a></td>-->
         <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
           <td><a href="<?php echo $this->url("usuario","edit"); ?>&id=<?php echo $value->id; ?>" class="btn btn-primary">Editar</a></td>
         <?php endif; ?>
       </tr>
  <?php endforeach; ?>
  </table>
  <?php
  else: ?>
    <h3 class="text-center">Sección exclusiva del usuario master</h3>
  <?php endif; ?>
</div>

<?php include('footer.php'); ?>
