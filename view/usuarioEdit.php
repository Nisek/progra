<?php include('head.php'); ?>
<div class="container">
    <?php if ($_SESSION['usuario']['id_roll'] == 1): ?>
  <h1>Editar Usuario</h1>
  <hr/>
  <div >

<form action="<?php echo $this->url("usuario","update"); ?>" method="post"  >

      <div class="row">
        <div class="col">
            <div class="form-group">
          Estado:
          <select name="statusSelect">
            <option value="0">Active</option>
            <option value="1">Inactive</option>
          </select>
        </div>
        </div>

        <div class="col">
          <div class="form-group">
          Logueado: <input type="text" name="logueado" class="form-control"required value="<?php echo $all[0]->logueado; ?> "/>
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="form-group">
          Roll: <input type="text" name="id_roll" class="form-control"required value="<?php echo $all[0]->id_roll; ?> "/>
          </div>

        </div>
        <div class="col">
          <div class="form-group">
          Nombre: <input type="text" name="nombre" class="form-control"required value="<?php echo $all[0]->nombre; ?> "/>
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="form-group">
          Apellido: <input type="text" name="apellido" class="form-control"required value="<?php echo $all[0]->apellido; ?> "/>
          </div>

        </div>
        <div class="col">
          <div class="form-group">
          Pass: <input type="password" name="pass" minlength="8" maxlength="25" required pattern="[A-Za-z0-9]+" class="form-control"
          title="La contrasena debe tener un mínimo de 8 caracteres conteniendo letras y números." value="<?php echo $all[0]->pass; ?>"/>
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="form-group">
          Cedula: <input type="text" name="cedula" class="form-control"required value="<?php echo $all[0]->cedula; ?> "/>
          </div>

        </div>
        <div class="col">
          <div class="form-group">
          NickName: <input type="text" name="nickname" class="form-control"required value="<?php echo $all[0]->nickname; ?> "/>
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col">
          <input type="hidden" name="id" value="<?php echo $all[0]->id; ?> " />
          <input type="submit" value="enviar" class="btn btn-success" />

        </div>
        <div class="col">


        </div>
      </div>

<br>


    </form>
  </div>
  <?php
  else: ?>
    <h3 class="text-center">Sección exclusiva del usuario master</h3>
  <?php endif; ?>
</div>

<?php include('footer.php'); ?>
