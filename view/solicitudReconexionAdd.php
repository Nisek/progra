<?php include('head.php'); ?>
<div class="container">
  <h2 class="text-center">Lista de Solicitudes de reconexiones</h2>
  <hr/>
  <table class="table table-default">
    <tr>
      <td>Cliente</td>
      <td>Estado</td>
      <td>Nombre</td>
      <td>Descripción</td>
      <td>Periodo</td>
    </tr>
    <?php if (isset($all)) { ?>
  <?php foreach ($all as $key => $value): ?>
       <tr>
         <td><?php echo $value->nombre; ?> <?php echo $value->apellido; ?></td>
         <td><?php echo $value->status; ?></td>
         <td><?php echo $value->nombreServicio; ?></td>
         <td><?php echo $value->descripcion; ?></td>
         <td><?php echo $value->nombrePeriodo; ?></td>

         <?php if ($_SESSION['usuario']['id_roll'] != 3): ?>
           <td><a href="<?php echo $this->url("solicitudReconexion","edit"); ?>&id=<?php echo $value->id; ?>" class="btn btn-primary">Recibido</a></td>
         <?php endif; ?>
       </tr>
  <?php endforeach; ?>
  <?php } ?>
  </table>


</div>



<?php include('footer.php'); ?>
