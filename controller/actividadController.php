<?php
class actividadController extends cls_view
{
  public $view = "actividad";
  function __construct()
  {
  }
  public function salir(){
    session_start();
    unset($_SESSION['usuario']);
    $this->redirect();
  }

  //metodo index
  public function index(){
    $myModel  = new cls_actividad();//$id,$nombre,$descripcion,$status,$action
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Index",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }

}

?>
