<?php
class clienteController extends cls_view
{
  public $view = "cliente";
  function __construct()
  {
  }
  //metodo index
  public function index(){
    $myModel  = new cls_cliente();//$id,$nombre,$descripcion,$status,$action
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Index",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo update
  public function update(){
    $values   = $this->valida();
    $myModel  = new cls_cliente();
    $myModel->setId($values["id"]);
    $myModel->setNombre($values["nombre"]);
    $myModel->setApellido($values["apellido"]);
    $myModel->setPass($values["pass"]);
    $myModel->setCedula($values["cedula"]);
    $myModel->setStatus($values["statusSelect"]);
    $myModel->setId_roll($values["id_roll"]);
    $myModel->setLogueado($values["logueado"]);
    $myModel->setNickname($values["nickname"]);

    $res = $myModel->update();
    if ($res) {
     $this->redirect("cliente", "show");
    }
    else {
      echo "Error al actualizar";
    }
  }


  //metodo edit
  public function edit(){
    if (isset($_GET["id"])) {
      $id = $_GET["id"];
      $myModel  = new cls_cliente();//$id,$nombre,$descripcion,$status,$action
      $all      = $myModel->getBy($id);
      $this->view(
        $this->view."Edit",
        array(
          "all" =>$all,
          "Hola"=>"Endesa Electricidad"
        )
      );
    }
  }
  //metodo add
  public function add(){
    $myModel  = new cls_cliente();//$id,$nombre,$descripcion,$status,$action
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Add",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo show
  public function show(){
    $myModel  = new cls_cliente();//$id,$nombre,$descripcion,$status,$action
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Show",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo insert
  public function insert(){//TODO CAMBIAR.
    $values   = $this->valida();
    $myModel  = new cls_cliente();
    $pass = $this->generarPass($values["nombre"],$values["apellido"],$values["cedula"]);
    $myModel->setNombre($values["nombre"]);
    $myModel->setApellido($values["apellido"]);
    $myModel->setPass($pass);
    $myModel->setCedula($values["cedula"]);
    $myModel->setStatus($values["statusSelect"]);
    $myModel->setId_roll(3);
    $myModel->setLogueado(0);
    $myModel->setNickname($values["nickname"]);

    $res = $myModel->insert($myModel);
    if ($res) {
     $this->redirect("cliente", "show");
   }else {
     echo "Error al Guardar";
   }
  }

  public function generarPass($nombre, $apellido, $cedula) {
    $primerLetraNombre = substr($nombre,0,1);
    $tresPrimApe = substr($apellido,0,3);
    $cuatroUltCed = substr($cedula, -4);

    $nuevoPass = $primerLetraNombre . $tresPrimApe . $cuatroUltCed;

    return $nuevoPass;
  }

  public function valida(){
    $values = null;
    if (isset($_POST["id"]) && !empty($_POST["id"]) && !is_null($_POST["id"])) {
      $values['id'] = $_POST["id"];
    }else{
      $values['id'] = "";
    }
    if (isset($_POST["nombre"]) && !empty($_POST["nombre"]) && !is_null($_POST["nombre"])) {
      $values['nombre'] = $_POST["nombre"];
    }else{
      $values['nombre'] = "";
    }

    if (isset($_POST["pass"]) && !empty($_POST["pass"]) && !is_null($_POST["pass"])) {
      $values['pass'] = $_POST["pass"];
    }else{
      $values['pass'] = "";
    }

    if (isset($_POST["apellido"]) && !empty($_POST["apellido"]) && !is_null($_POST["apellido"])) {
      $values['apellido'] = $_POST["apellido"];
    }else{
      $values['apellido'] = "";
    }

    if (isset($_POST["cedula"]) && !empty($_POST["cedula"]) && !is_null($_POST["cedula"])) {
      $values['cedula'] = $_POST["cedula"];
    }else{
      $values['cedula'] = "";
    }

    if (isset($_POST["statusSelect"])) {
      $values['statusSelect'] = $_POST["statusSelect"];
    }else{
      $values['statusSelect'] = "";
    }

    if (isset($_POST["id_roll"]) && !empty($_POST["id_roll"]) && !is_null($_POST["id_roll"])) {
      $values['id_roll'] = $_POST["id_roll"];
    }else{
      $values['id_roll'] = "";
    }

    if (isset($_POST["logueado"]) && !empty($_POST["logueado"]) && !is_null($_POST["logueado"])) {
      $values['logueado'] = $_POST["logueado"];
    }else{
      $values['logueado'] = "";
    }

    if (isset($_POST["nickname"]) && !empty($_POST["nickname"]) && !is_null($_POST["nickname"])) {
      $values['nickname'] = $_POST["nickname"];
    }else{
      $values['nickname'] = "";
    }
    /*echo "<pre>";
    var_dump($values);
    echo "</pre>";*/
    return $values;
  }

}

?>
