<?php
class solicitudReconexionController extends cls_view
{
  public $view = "solicitudReconexion";
  function __construct()
  {
  }
  public function salir(){
    session_start();
    unset($_SESSION['usuario']);
    $this->redirect();
  }

  //metodo index
  public function index(){
    $this->view(
      $this->view."Index",
      array(
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo update
  public function update(){
    $values   = $this->valida();
    $myModel  = new cls_usuario();
    $myModel->setId($values["id"]);
    $myModel->setStatus(0);
    $myModel->setLogueado(0);
    $myModel->setId_roll($values["id_roll"]);
    $myModel->setNombre($values["nombre"]);
    $myModel->setApellido($values["apellido"]);
    $myModel->setPass($values["pass"]);
    $myModel->setCedula($values["cedula"]);
    $myModel->setNickname($values["nickname"]);
  }

  //metodo edit
  public function edit(){
    if (isset($_GET["id"])) {
      $id = $_GET["id"];
      $myModel  = new cls_solicitudReconexion();
      $all      = $myModel->update($id);

      $res = $myModel->getIdDetalleServicio($id);
      foreach ($res as $key => $value)
      {
        $idDetalle =$value->id_detalle_servicio;
        $myModel->updateMulta($idDetalle);
        $myModel->updateEstado($idDetalle);
        $myModel->updateDetalleServicioPeriodo($idDetalle);
      }

      if ($all) {
      $this->redirect("solicitudReconexion", "add");
    }else {
      echo "Error al guardar";
    }
    }
  }

  //metodo add
  public function add(){
    $myModel  = new cls_solicitudReconexion();
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Add",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }


  public function valida(){
    $values = null;
    if (isset($_POST["id"]) && !empty($_POST["id"]) && !is_null($_POST["id"])) {
      $values['id'] = $_POST["id"];
    }else{
      $values['id'] = "";
    }

    if (isset($_POST["status"]) && !empty($_POST["status"]) && !is_null($_POST["status"])) {
      $values['status'] = $_POST["status"];
    }else{
      $values['status'] = "";
    }

    if (isset($_POST["logueado"]) && !empty($_POST["logueado"]) && !is_null($_POST["logueado"])) {
      $values['logueado'] = $_POST["logueado"];
    }else{
      $values['logueado'] = "";
    }

    if (isset($_POST["id_roll"]) && !empty($_POST["id_roll"]) && !is_null($_POST["id_roll"])) {
      $values['id_roll'] = $_POST["id_roll"];
    }else{
      $values['id_roll'] = "";
    }

    if (isset($_POST["nombre"]) && !empty($_POST["nombre"]) && !is_null($_POST["nombre"])) {
      $values['nombre'] = $_POST["nombre"];
    }else{
      $values['nombre'] = "";
    }
    if (isset($_POST["apellido"]) && !empty($_POST["apellido"]) && !is_null($_POST["apellido"])) {
      $values['apellido'] = $_POST["apellido"];
    }else{
      $values['apellido'] = "";
    }

    if (isset($_POST["cedula"]) && !empty($_POST["cedula"]) && !is_null($_POST["cedula"])) {
      $values['cedula'] = $_POST["cedula"];
    }else{
      $values['cedula'] = "";
    }

    if (isset($_POST["nickname"]) && !empty($_POST["nickname"]) && !is_null($_POST["nickname"])) {
      $values['nickname'] = $_POST["nickname"];
    }else{
      $values['nickname'] = "";
    }


    return $values;
  }

}

?>
