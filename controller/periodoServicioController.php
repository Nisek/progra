<?php
class periodoServicioController extends cls_view
{
  public $view = "periodoServicio";
  function __construct()
  {
  }
  //metodo index
  public function index(){
    $myModel  = new cls_periodoServicio();
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Index",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }


  public function update(){
    $values   = $this->valida();
    $myModel  = new cls_periodoServicio();
    $myModel->setid($values["id"]);
    $myModel->setid_periodo($values["id_periodo"]);
    $myModel->setid_detalle_servicio($values["id_detalle_servicio"]);
    $myModel->settotal($values["total"]);
    $myModel->setpago($values["pago"]);
    $myModel->setid_multa($values["id_multa"]);
    $myModel->setStatus($values["statusSelect"]);

    $res = $myModel->update();
    if ($res) {
     $this->redirect("periodoServicio", "show");
    }
  }
  //metodo edit
  public function edit(){
    if (isset($_GET["id"])) {
      $id = $_GET["id"];
      $myModel  = new cls_periodoServicio();
      $all      = $myModel->getBy($id);
      $this->view(
        $this->view."Edit",
        array(
          "all" =>$all,
          "Hola"=>"Endesa Electricidad"
        )
      );
    }
  }
  //metodo add
  public function add(){
    $myModel  = new cls_periodoServicio();
    $all      = $myModel->getAll();
    $myModel2  = new cls_periodo();
    $periodosX= $myModel2 ->getAll();
    $this->view(
      $this->view."Add",
      array(
        "all" =>$all,
        "periodosX" => $periodosX,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo show
  public function show(){
    $myModel  = new cls_periodoServicio();
    $all      = $myModel->getAllDatos();
    $this->view(
      $this->view."Show",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo insert
  public function insert(){
    $values   = $this->valida();
    $myModel  = new cls_periodoServicio();
    $myModel->setid($values["id"]);
    $myModel->setid_periodo($values["id_periodo"]);
    $myModel->setid_detalle_servicio($values["id_detalle_servicio"]);
    $myModel->settotal($values["total"]);
    $res = $myModel->insert($myModel);
    if ($res) {
     $this->redirect("periodoServicio", "show");
    }
  }
  public function valida(){
    $values = null;
    if (isset($_POST["id"]) && !empty($_POST["id"]) && !is_null($_POST["id"])) {
      $values['id'] = $_POST["id"];
    }else{
      $values['id'] = "";
    }
    if (isset($_POST["id_periodo"]) && !empty($_POST["id_periodo"]) && !is_null($_POST["id_periodo"])) {
      $values['id_periodo'] = $_POST["id_periodo"];
    }else{
      $values['id_periodo'] = "";
    }
    if (isset($_POST["id_detalle_servicio"]) && !empty($_POST["id_detalle_servicio"]) && !is_null($_POST["id_detalle_servicio"])) {
      $values['id_detalle_servicio'] = $_POST["id_detalle_servicio"];
    }else{
      $values['id_detalle_servicio'] = "";
    }
    if (isset($_POST["total"]) && !empty($_POST["total"]) && !is_null($_POST["total"])) {
      $values['total'] = $_POST["total"];
    }else{
      $values['total'] = "";
    }
    if (isset($_POST["statusSelect"])) {
      $values['statusSelect'] = $_POST["statusSelect"];
    }else{
      $values['statusSelect'] = "";
    }
    /*echo "<pre>";
    var_dump($values);
    echo "</pre>";*/
    return $values;
  }

}

?>
