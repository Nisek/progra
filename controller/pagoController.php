<?php
class pagoController extends cls_view
{
  public $view = "pago";
  function __construct()
  {
  }
  //metodo index
  public function index(){
    $this->view(
      $this->view."Index",
      array(
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo update
  public function update(){
    $values   = $this->valida();
    $myModel  = new cls_pago();
    $myModel->setId($values["id"]);
    $myModel->setNombre($values["nombre"]);
    $myModel->setfecha_inicio($values["fecha_inicio"]);
    $myModel->setfecha_fin($values["fecha_fin"]);
    $myModel->setStatus($values["statusSelect"]);

    $res = $myModel->update();
    if ($res) {
     $this->redirect("pago", "show");
    }
  }
  //metodo edit
  public function edit(){
    if (isset($_GET["id"])) {
      $id = $_GET["id"];
      $myModel  = new cls_pago();
      $all      = $myModel->getBy($id);
      $this->view(
        $this->view."Edit",
        array(
          "all" =>$all,
          "Hola"=>"Endesa Electricidad"
        )
      );
    }
  }
  //metodo add
  public function add(){
    $myModel  = new cls_pago();
    $myModel->setIdUsuario($_SESSION['usuario']['id']);
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Add",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo show
  public function show(){
    $myModel  = new cls_pago();//$id,$nombre,$descripcion,$status,$action
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Show",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo insert
  public function insert(){
    $values   = $this->valida();
    $myModel  = new cls_pago();
    $myModel->setIdParaPago($values["id_Para_Pagar"]);
    $res = $myModel->insert($myModel);
    if ($res) {
     $this->redirect("pago", "index");
    }
  }
  public function valida(){
    $values = null;
    if (isset($_POST["id_Para_Pagar"]) && !empty($_POST["id_Para_Pagar"]) && !is_null($_POST["id_Para_Pagar"])) {
      $values['id_Para_Pagar'] = $_POST["id_Para_Pagar"];
    }else{
      $values['id_Para_Pagar'] = "";
    }

    return $values;
  }

}

?>
