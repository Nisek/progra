<?php
class detalleController extends cls_view
{
  public $view = "detalle";
  function __construct()
  {
  }
  public function salir(){
    session_start();
    unset($_SESSION['usuario']);
    $this->redirect();
  }

  //metodo index
  public function index(){
    $myModel  = new cls_detalle();//$id,$nombre,$descripcion,$status,$action
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Index",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo update
  public function update(){
    $values   = $this->valida();
    $myModel  = new cls_detalle();
    $myModel->setID($values["ID"]);
    $myModel->setId_servicio($values["id_servicio"]);
    $myModel->setNombre($values["nombre"]);
    $myModel->setDescripcion($values["descripcion"]);
    $myModel->setPrecio($values["precio"]);
    $myModel->setStatus($values["status"]);

    //$myModel->setStatus($values["statusSelect"]);

    $res = $myModel->update();
    if ($res) {
     $this->redirect("detalle", "show");
    }
    else {
      echo "Error al guardar";
  }
  }
  //metodo edit
  public function edit(){
    if (isset($_GET["ID"])) {
      $ID = $_GET["ID"];
      $myModel2  = new cls_servicio();
      $myModel  = new cls_detalle();//$id,$nombre,$descripcion,$status,$action
      $all      = $myModel->getBy($ID);
      $all2      = $myModel2->getAll();
      $this->view(
        $this->view."Edit",
        array(
          "elserviciogg" => $all2,
          "all" =>$all,
          "Hola"=>"Endesa Electricidad"
        )
      );
    }
  }
  //metodo add
  public function add(){
    $myModel  = new cls_servicio();//$id,$nombre,$descripcion,$status,$action
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Add",
      array(
        "IDS" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo show
  public function show(){
    $myModel  = new cls_detalle();//$id,$nombre,$descripcion,$status,$action
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Show",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }

  //metodo show servicios para un cliente especifico
  public function showMisServicios(){
    $myModel  = new cls_detalle();
    $myModel->setIdUsuario($_SESSION['usuario']['id']);
    $all      = $myModel->getAllMisServicios();
    $this->view(
      $this->view."ListaServiciosShow",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }

  public function showMisServiciosActivos(){
    $myModel  = new cls_detalle();
    $myModel->setIdUsuario($_SESSION['usuario']['id']);
    $all      = $myModel->getAllMisServiciosActivos();
    $this->view(
      $this->view."ListaServiciosActivosShow",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }



  //metodo insert
  public function insert(){//TODO CAMBIAR.
    $values   = $this->valida();
    $myModel  = new cls_detalle();
    $myModel->setId_servicio($values["id_servicio"]);
    $myModel->setNombre($values["nombre"]);
    $myModel->setDescripcion($values["descripcion"]);
    $myModel->setPrecio($values["precio"]);
    //$myModel->setStatus($values["status"]);
    $myModel->setStatus(0);
    //$myModel->setStatus($values["statusSelect"]);

      $res = $myModel->insert($myModel);
      if ($res) {
      $this->redirect("detalle", "show");
    }else {
      echo "Error al guardar";
    }
    }
    public function valida(){
      $values = null;
    if (isset($_POST["ID"]) && !empty($_POST["ID"]) && !is_null($_POST["ID"])) {
      $values['ID'] = $_POST["ID"];
    }else{
      $values['ID'] = "";
    }

    if (isset($_POST["id_servicio"]) && !empty($_POST["id_servicio"]) && !is_null($_POST["id_servicio"])) {
      $values['id_servicio'] = $_POST["id_servicio"];
    }else{
      $values['id_servicio'] = "";
    }

    if (isset($_POST["nombre"]) && !empty($_POST["nombre"]) && !is_null($_POST["nombre"])) {
      $values['nombre'] = $_POST["nombre"];
    }else{
      $values['nombre'] = "";
    }

    if (isset($_POST["descripcion"]) && !empty($_POST["descripcion"]) && !is_null($_POST["descripcion"])) {
      $values['descripcion'] = $_POST["descripcion"];
    }else{
      $values['descripcion'] = "";
    }

    if (isset($_POST["precio"]) && !empty($_POST["precio"]) && !is_null($_POST["precio"])) {
      $values['precio'] = $_POST["precio"];
    }else{
      $values['precio'] = "";
    }
    if (isset($_POST["status"]) && !empty($_POST["status"]) && !is_null($_POST["status"])) {
      $values['status'] = $_POST["status"];
    }else{
      $values['status'] = "";
    }

    /*if (isset($_POST["statusSelect"]) && !empty($_POST["statusSelect"]) && !is_null($_POST["statusSelect"])) {
      $values['statusSelect'] = $_POST["statusSelect"];
    }else{
      $values['statusSelect'] = "";
    } */
    return $values;
  }

}

?>
