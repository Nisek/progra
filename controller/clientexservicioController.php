<?php

class clientexservicioController extends cls_view
{
  public $view = "clientexservicio";
  function __construct()
  {
  }
  //metodo index
  public function index(){
    $myModel  = new cls_clientexservicio();
    $this->view(
      $this->view."Index",
      array(
        "all" =>null,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo update
  public function update(){
    $values   = $this->valida();
    $myModel  = new cls_clientexservicio();
    $myModel->setId($values["id"]);
    $myModel->setStatus($values["statusSelect"]);

    $res = $myModel->update();
    if ($res) {
    $this->redirect("clientexservicio", "show");
    }
    else {
      echo "Error al actualizar";
    }
  }


  //metodo edit
  public function edit(){
    if (isset($_GET["id"])) {
      $id = $_GET["id"];
      $myModel  = new cls_clientexservicio();//$id,$nombre,$descripcion,$status,$action
      $all      = $myModel->getBy($id);
      $this->view(
        $this->view."Edit",
        array(
          "all" =>$all,
          "Hola"=>"Endesa Electricidad"
        )
      );
    }
  }
  //metodo add
  public function add(){
    $myModel  = new cls_detalle();
    $all1      = $myModel->getAllAsignadosAPeriodos();
    $myModel  = new cls_cliente();
    $all2      = $myModel->getAll();
    $this->view(
      $this->view."Add",
      array(
        "serviciosg" =>$all1,
        "clientesg" =>$all2,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo show
  public function show(){
    $myModel  = new cls_clientexservicio();//$id,$nombre,$descripcion,$status,$action
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Show",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo insert
  public function insert(){//TODO CAMBIAR.
   $values   = $this->valida();
    $myModel  = new cls_clientexservicio();

    $myModel->setIdCliente($values["idCliente"]);
    $myModel->setIdDetalleServicio($values["idServicio"]);
    $myModel->setStatus($values["statusSelect"]);

    $res = $myModel->insertClientePorServicio($myModel);

    if ($res) {
      $res2 = $myModel->updateDelClientePorServicio($mymodel);
      if($res2)
      {
      $this->redirect("clientexservicio", "show");
    }
    else {
      echo "Error al Guardar";
    }
    }

  }

  public function valida(){
    $values = null;
    if (isset($_POST["IdCliente"]) && !empty($_POST["IdCliente"]) && !is_null($_POST["IdCliente"])) {
      $values['idCliente'] = $_POST["IdCliente"];
    }else{
      $values['idCliente'] = "";
    }
  if (isset($_POST["IdDetalleServicio"]) && !empty($_POST["IdDetalleServicio"]) && !is_null($_POST["IdDetalleServicio"])) {
      $values['idServicio'] = $_POST["IdDetalleServicio"];
    }else{
      $values['idServicio'] = "";
    }

    if (isset($_POST["id"]) && !empty($_POST["id"]) && !is_null($_POST["id"])) {
        $values['id'] = $_POST["id"];
      }else{
        $values['id'] = "";
      }

    if (isset($_POST["statusSelect"])) {
      $values['statusSelect'] = $_POST["statusSelect"];
    }else{
      $values['statusSelect'] = "";
    }
    /*echo "<pre>";
    var_dump($values);
    echo "</pre>";*/
    return $values;
  }

}

?>
