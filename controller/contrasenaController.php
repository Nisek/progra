<?php
class contrasenaController extends cls_view
{
  public $view = "contrasena";
  function __construct()
  {
  }

  //metodo index
  public function index(){
    $all      = "Debe realizar el cambio de contraseña para continuar.";
    $this->view(
      $this->view."Index",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }

  public function insert(){//TODO CAMBIAR.
    $values   = $this->valida();
    $myModel  = new cls_contrasena();
    $myModel->setPass($values["nuevoPass"]);
    $myModel->setId($values["id"]);
    $myModel->setId_roll($values["id_roll"]);

    $res = $myModel->update($myModel);
    if ($res) {
     $_SESSION['usuario']['reqCambio'] = 0;
     $_SESSION['usuario']['pass'] = $values["nuevoPass"];
     $this-> redirect()->home();
    }
  }
  public function valida(){
    $values = null;
    if (isset($_POST["id"]) && !empty($_POST["id"]) && !is_null($_POST["id"])) {
      $values['id'] = $_POST["id"];
    }else{
      $values['id'] = "";
    }
    if (isset($_POST["nuevoPass"]) && !empty($_POST["nuevoPass"]) && !is_null($_POST["nuevoPass"])) {
      $values['nuevoPass'] = $_POST["nuevoPass"];
    }else{
      $values['nuevoPass'] = "";
    }
    if (isset($_POST["id_roll"]) && !empty($_POST["id_roll"]) && !is_null($_POST["id_roll"])) {
      $values['id_roll'] = $_POST["id_roll"];
    }else{
      $values['id_roll'] = "";
    }

    return $values;
  }

}

?>
