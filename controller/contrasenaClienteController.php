<?php
class contrasenaClienteController extends cls_view
{
  public $view = "contrasena";
  function __construct()
  {
  }

  //metodo index
  public function index(){
    $all      = "Debe realizar el cambio de contraseña para continuar.";
    $this->view(
      $this->view."Change",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }

  public function insert(){
    $values   = $this->valida();
    $myModel  = new cls_contrasena();

    $ced = $_SESSION['usuario']['cedula'];



   $myModel->setCedula($ced);
    $res = $myModel->buscarCliente();

     if (!is_null($res)) {
      $passw =  $res[0]->pass;
      $nuevoPass = $values["contrasena"];

      if($passw == $nuevoPass)
      {
        $myModel->setPass($values["nuevoPass"]);
        $myModel->setId($values["id"]);
        $myModel->setId_roll($values["id_roll"]);

        $res = $myModel->update($myModel);
        if ($res) {
         $_SESSION['usuario']['reqCambio'] = 0;
         $_SESSION['usuario']['pass'] = $values["nuevoPass"];
         $this-> redirect()->home();
        }
        else {
          echo "Ocurrió un error, verifique que la contraseña actual ingresada sea la correcta.";
        }
      }
      else {
        echo "Ocurrió un error, verifique que la contraseña actual ingresada sea la correcta.";
      }
     }
     else {
       echo "Ocurrió un error, verifique que la contraseña actual ingresada sea la correcta."; 
     }
  }


  public function valida(){
    $values = null;
    if (isset($_POST["id"]) && !empty($_POST["id"]) && !is_null($_POST["id"])) {
      $values['id'] = $_POST["id"];
    }else{
      $values['id'] = "";
    }
    if (isset($_POST["contrasena"]) && !empty($_POST["contrasena"]) && !is_null($_POST["contrasena"])) {
      $values['contrasena'] = $_POST["contrasena"];
    }else{
      $values['contrasena'] = "";
    }
    if (isset($_POST["nuevoPass"]) && !empty($_POST["nuevoPass"]) && !is_null($_POST["nuevoPass"])) {
      $values['nuevoPass'] = $_POST["nuevoPass"];
    }else{
      $values['nuevoPass'] = "";
    }
    if (isset($_POST["id_roll"]) && !empty($_POST["id_roll"]) && !is_null($_POST["id_roll"])) {
      $values['id_roll'] = $_POST["id_roll"];
    }else{
      $values['id_roll'] = "";
    }

    return $values;
  }

}

?>
