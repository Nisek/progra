<?php
class solicitudController extends cls_view
{
  public $view = "solicitud";
  function __construct()
  {
  }
  public function salir(){
    session_start();
    unset($_SESSION['usuario']);
    $this->redirect();
  }

  //metodo index
  public function index(){
    $this->view(
      $this->view."Index",
      array(
        "Hola"=>"Endesa Electricidad"
      )
    );
  }

  //metodo add
  public function add(){
    $myModel  = new cls_solicitud();
    $myModel->setIdUsuario($_SESSION['usuario']['id']);
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Add",
      array(
        "all" =>$all,
      )
    );
  }
  //metodo show
  public function show(){
    $myModel  = new cls_solicitud();
    $myModel->setIdUsuario($_SESSION['usuario']['id']);
    $all      = $myModel->getAllReconexiones();
    $this->view(
      $this->view."Show",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }

  //metodo insert
  public function insert(){
    $values   = $this->valida();
    $myModel  = new cls_solicitud();
    $myModel->setIdDetalleServicio($values["id_detalle_servicio"]);
    $myModel->setIdPeriodo($values['idPeriodo']);
    $myModel->setStatus(1);

      $res = $myModel->insert($myModel);
      if ($res) {
      $this->redirect("solicitud", "show");
    }else {
      echo "Error al guardar";
    }
    }
    public function valida(){
      $values = null;
    if (isset($_POST["id_detalle_servicio"]) && !empty($_POST["id_detalle_servicio"]) && !is_null($_POST["id_detalle_servicio"])) {
      $values['id_detalle_servicio'] = $_POST["id_detalle_servicio"];
    }else{
      $values['id_detalle_servicio'] = "";
    }

    if (isset($_POST["idDelPeriodo"]) && !empty($_POST["idDelPeriodo"]) && !is_null($_POST["idDelPeriodo"])) {
      $values['idPeriodo'] = $_POST["idDelPeriodo"];
    }else{
      $values['idPeriodo'] = "";
    }
    return $values;
  }
}

?>
