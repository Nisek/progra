<?php
class periodoController extends cls_view
{
  public $view = "periodo";
  function __construct()
  {
  }
  //metodo index
  public function index(){
    $myModel  = new cls_periodo();
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Index",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo update
  public function update(){
    $values   = $this->valida();
    $myModel  = new cls_periodo();
    $myModel->setId($values["id"]);
    $myModel->setStatus($values["statusSelect"]);

    $res = $myModel->update();
    if ($values["statusSelect"] = 1) {  //inactivó
      $res2 = $myModel->getInactivadosPeriodos();
        foreach ($res2 as $key => $value)
        {
          $idDetalle =$value->idNoPago;
          $myModel->updateMulta($idDetalle);
          $myModel->updateEstado($idDetalle);
          $myModel->updateDetalleServicioPeriodo($idDetalle);
        }
    }
    if ($res && $res2) {
     $this->redirect("periodo", "show");
    }
    else {
      $this->redirect("periodo", "show");
    }
  }
  //metodo edit
  public function edit(){
    if (isset($_GET["id"])) {
      $id = $_GET["id"];
      $myModel  = new cls_periodo();
      $all      = $myModel->getBy($id);
      $this->view(
        $this->view."Edit",
        array(
          "all" =>$all,
          "Hola"=>"Endesa Electricidad"
        )
      );
    }
  }
  //metodo add
  public function add(){
    $myModel  = new cls_periodo();
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Add",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo show
  public function show(){
    $myModel  = new cls_periodo();//$id,$nombre,$descripcion,$status,$action
    $all      = $myModel->getAll();
    $this->view(
      $this->view."Show",
      array(
        "all" =>$all,
        "Hola"=>"Endesa Electricidad"
      )
    );
  }
  //metodo insert
  public function insert(){//TODO CAMBIAR.
    $values   = $this->valida();
    $myModel  = new cls_periodo();
    $myModel->setId($values["id"]);
    $myModel->setNombre($values["nombre"]);
    $myModel->setFecha_inicio($values["fecha_inicio"]);
    $myModel->setFecha_fin($values["fecha_fin"]);
    $myModel->setStatus($values["statusSelect"]);

    $res = $myModel->insert($myModel);
    if ($res) {
     $this->redirect("periodo", "show");
    }
  }
  public function valida(){
    $values = null;
    if (isset($_POST["id"]) && !empty($_POST["id"]) && !is_null($_POST["id"])) {
      $values['id'] = $_POST["id"];
    }else{
      $values['id'] = "";
    }
    if (isset($_POST["nombre"]) && !empty($_POST["nombre"]) && !is_null($_POST["nombre"])) {
      $values['nombre'] = $_POST["nombre"];
    }else{
      $values['nombre'] = "";
    }
    if (isset($_POST["fecha_inicio"]) && !empty($_POST["fecha_inicio"]) && !is_null($_POST["fecha_inicio"])) {
      $values['fecha_inicio'] = $_POST["fecha_inicio"];
    }else{
      $values['fecha_inicio'] = "";
    }
    if (isset($_POST["fecha_fin"]) && !empty($_POST["fecha_fin"]) && !is_null($_POST["fecha_fin"])) {
      $values['fecha_fin'] = $_POST["fecha_fin"];
    }else{
      $values['fecha_fin'] = "";
    }
    if (isset($_POST["statusSelect"])) {
      $values['statusSelect'] = $_POST["statusSelect"];
    }else{
      $values['statusSelect'] = "";
    }

    return $values;
  }

}

?>
