<?php
include('../config/global.php');
include('../core/cls_conexion.php');
include('../core/cls_view.php');
include('../model/cls_contrasena.php');
include('../controller/contrasenaController.php');

$myModel = new cls_contrasena();
$myModel->setCedula($_POST['cedulaV']);
$res = $myModel->buscarUsuario();

if (!is_null($res)) {
  $primerLetraNombre = substr($res[0]->nombre,0,1);
  $tresPrimApe = substr($res[0]->apellido,0,3);
  $cuatroUltCed = substr($res[0]->cedula, -4);

  $pass = $primerLetraNombre . $tresPrimApe . $cuatroUltCed;

  $myModel->setId($res[0]->id);
  $myModel->setPass($pass);
  $res2 = $myModel->updateUsuario();

  if ($res2) {
    echo'<script type="text/javascript">
    alert("Se ha realizado el cambio de contraseña correctamente, por favor ingrese con la contraseña autogenerada.");
    window.location.href="index.php";
    </script>';
  }
  else {
    echo'<script type="text/javascript">
    alert("Ha ocurrido un error al cambiar la contraseña, verifique la cédula digitada, si el problema persiste comuníquese con el administrador.");
    window.location.href="index.php";
    </script>';
  }
 }
 elseif (is_null($res)) {
   $res3 = $myModel->buscarCliente();

   if (!is_null($res3)) {
     $primerLetraNombre = substr($res3[0]->nombre,0,1);
     $tresPrimApe = substr($res3[0]->apellido,0,3);
     $cuatroUltCed = substr($res3[0]->cedula, -4);

     $pass = $primerLetraNombre . $tresPrimApe . $cuatroUltCed;

     $myModel->setId($res3[0]->id);
     $myModel->setPass($pass);

     $res4 = $myModel->updateCliente();

     if ($res4) {
       echo'<script type="text/javascript">
       alert("Se ha realizado el cambio de contraseña correctamente, por favor ingrese con la contraseña autogenerada.");
       window.location.href="index.php";
       </script>';
     }
     else {
       echo'<script type="text/javascript">
       alert("Ha ocurrido un error al cambiar la contraseña, verifique la cédula digitada, si el problema persiste comuníquese con el administrador.");
       window.location.href="index.php";
       </script>';
     }
   }
   else {
     echo'<script type="text/javascript">
     alert("Ha ocurrido un error al cambiar la contraseña, verifique la cédula digitada, si el problema persiste comuníquese con el administrador.");
     window.location.href="index.php";
     </script>';
   }
 }
 else {
   echo'<script type="text/javascript">
   alert("Ha ocurrido un error al cambiar la contraseña, verifique la cédula digitada, si el problema persiste comuníquese con el administrador.");
   window.location.href="index.php";
   </script>';
 }




?>
