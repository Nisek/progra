
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
  </head>
  <body>
    <header>
      <!--Navbar-->
      <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
        <div class="container">
          <a class="navbar-brand" href="#"><strong>ENDESA</strong></a>
          <button type="button"  data-toggle="modal" class="btn btn-info" data-target="#myModal" class="nav justify-content-end">Iniciar Sesión </button>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
            aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
          </div>
        </div>
      </nav>
            <div class="bd-example">
          <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="../img/electricidad.jpg" class="d-block w-100" alt="50">
                <div class="carousel-caption d-none d-md-block">
                  <h1>Electricidad</h1>
                  <p>Contamos con gran cobertura a lo largo del pais.</p>
                </div>
              </div>
              <div class="carousel-item">
                <img src="../img/internet.jpg" class="d-block w-100" alt="50">
                <div class="carousel-caption d-none d-md-block">
                  <h1>Internet</h1>
                  <p>La mejor velocidad del mercado.</p>
                </div>
              </div>
              <div class="carousel-item">
                <img src="../img/cable.png" class="d-block w-100" alt="50">
                <div class="carousel-caption d-none d-md-block">
                  <h1>Cable</h1>
                  <p>Contamos con gran variedad de canales para toda la familia.</p>
                </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!--- Modal content-->
        <div class="modal-content">
          <div class="modal-header">

            <h4 class="modal-title">Inicio de sesión</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">

            <form class="container" action="valida.php" method="post">
              <div class="form-group">
                <label>Nombre de Usuario:</label>
             <input type="text" required name="nickname" id="nickname" class="form-control"/>
              </div>
              <div class="form-group">
                <label>Contraseña:</label><br>
               <input type="password" required name="pass" id="pass" class="form-control" />

                <input type="checkbox" name="recordar" id="recordar" value="">Recuerdame
                <a href="#" class="btn btn-default"></a>
                <br>
                <a href="#OlvidoContra"data-toggle="modal" type="button" >¿Olvido su contraseña?</a>

              </div>
              <br>

              <input type="submit" value="Iniciar" onclick="validarCookie()" class="btn btn-success" />
            </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>

      </div>
    </div>

    <div id="OlvidoContra" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">

            <h4 class="modal-title">Solicitud de cambio de Contraseña</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
  <br>
            <form class="container" action="regenera.php" method="post">
              <div class="form-group">
                <label>Código de usuario (cédula)</label>
             <input type="text" name="cedulaV" class="form-control" value=""/>
              </div>
              <br>

              <input type="submit" value="Generar Contraseña" class="btn btn-success" />
            </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>

      </div>
    </div>

  </body>
<footer class="page-footer font-small blue">
  <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="#"> Progra3-2019</a>
  </div>
</footer>
</html>


<script>

window.onload = function() {
  checkCookie();
}

function validarCookie() {
  var isChecked = document.getElementById('recordar').checked;
  var campoUsuario = $('#nickname').val();
  var campoPass = $('#pass').val();
  if(isChecked & campoUsuario != '' & campoPass != ''){
    document.cookie = "usuario=" + encodeURIComponent(campoUsuario);
    document.cookie = "pass=" + encodeURIComponent(campoPass);
  }
 }

function checkCookie(){
  var user = readCookie( "usuario" );
  var pass = readCookie( "pass" );
    if (user != "" & user != null & pass != "" & pass != null) {
      document.getElementById("nickname").value = user;
      document.getElementById("pass").value = pass;
    }
}


 function readCookie(name) {

   var nameEQ = name + "=";
   var ca = document.cookie.split(';');

   for(var i=0;i < ca.length;i++) {

     var c = ca[i];
     while (c.charAt(0)==' ') c = c.substring(1,c.length);
     if (c.indexOf(nameEQ) == 0) {
       return decodeURIComponent( c.substring(nameEQ.length,c.length) );
     }

   }

   return null;

 }
/*function cargarDatos(){
  var user =[ <?php echo $usuario;?> ];
  document.getElementById("nickname").value = user;
  document.getElementById("pass").value = "12345678";
}*/
</script>
