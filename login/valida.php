<?php
include('../config/global.php');
include('../core/cls_conexion.php');
include('../core/cls_view.php');
include('../model/cls_usuario.php');
include('../model/cls_cliente.php');
include('../controller/usuarioController.php');
$myModel = new cls_usuario();
$myModel->setNickname($_POST['nickname']);
$myModel->setPass($_POST['pass']);
$res = $myModel->login();

if (!is_null($res)) {

  session_start();
  $miuser = array(
  	'id'       => $res[0]->id,
  	'status'   => $res[0]->status,
    'logueado' => $res[0]->logueado,
    'id_roll'  => $res[0]->id_roll,
    'nombre'   => $res[0]->nombre,
    'apellido' => $res[0]->apellido,
    'pass'     => $res[0]->pass,
    'cedula'   => $res[0]->cedula,
    'nickname' => $res[0]->nickname,
    'reqCambio'=> $res[0]->requiereCambioPass
    ,
  	'time'     => time()+60000000000
  	 );
  $_SESSION['usuario'] = $miuser;
  header("Location: ../");
}
else {
  $myModel2 = new cls_cliente();
  $myModel2->setNickname($_POST['nickname']);
  $myModel2->setPass($_POST['pass']);
  $res2 = $myModel2->login();

  if (!is_null($res2)) {
    session_start();
    $miuser2 = array(
    	'id'       => $res2[0]->id,
    	'status'   => $res2[0]->status,
      'logueado' => $res2[0]->logueado,
      'id_roll'  => $res2[0]->id_roll,
      'nombre'   => $res2[0]->nombre,
      'apellido' => $res2[0]->apellido,
      'pass'     => $res2[0]->pass,
      'cedula'   => $res2[0]->cedula,
      'nickname' => $res2[0]->nickname,
      'reqCambio'=> $res2[0]->requiereCambioPass
      ,
    	'time'     => time()+60000000000
    	 );

    $_SESSION['usuario'] = $miuser2;

      header("Location: ../");

   }
   else{
     echo '<script>alert("Nombre de usuario o contraseña inválidos.");parent.location = "index.php"</script>';
   }
}
?>
