<?php

class cls_solicitud extends cls_conexion
{
  private $id, $id_detalle_servicio, $idPeriodo, $status;

  private $table = "reconexion";
  function __construct()
  {
  }
  public function getIdUsuario(){return $this->id;}
  public function setIdUsuario($param){$this->id=$param;}

  public function getIdDetalleServicio(){return $this->id_detalle_servicio;}
  public function setIdDetalleServicio($param){$this->id_detalle_servicio=$param;}
  public function getIdPeriodo(){return $this->$idPeriodo;}
  public function setIdPeriodo($param){$this->$idPeriodo=$param;}
  public function getStatus(){return $this->status;}
  public function setStatus($param){$this->status=$param;}

  public function insert(){
    $ex   = false;
    $res  = null;
    $this->do_open();
    $query="
    INSERT INTO reconexion (id_detalle_servicio,id_periodo,status) VALUES (
    '".$this->getIdDetalleServicio()."',
    '".$this->getIdPeriodo()."',
    ".$this->getStatus()."
  );";
$actionlog = array(
  'query' => $query,
  'table' => $this->table,
  'action' => "insert"
);
$ex= false;
$res = $this -> do_actionlog($actionlog);
if ($res) {
  $ex = true;
}

    $this->do_close();
    return $ex;
  }
  public function getBy($_id){
    $query = "
    SELECT *
    FROM periodo
    WHERE
    id = ".$_id;
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;
  }
  public function getAll(){ //lo uso
    $query =
    "SELECT t1.id AS idDelCxDS, t1.id_detalle_servicio, t2.nombre as nombreDS,
    t2.descripcion, t3.id_periodo, t4.nombre AS nombrePeriodo,t3.pago
    FROM endesa.cliente_x_detalle_servicio t1
    INNER JOIN endesa.detalle_servicio t2
    ON t1.id_detalle_servicio = t2.ID
    INNER JOIN endesa.detalle_servicio_x_periodo t3
    ON t3.id_detalle_servicio = t2.ID
    INNER JOIN endesa.periodo t4
    ON t3.id_periodo = t4.id
    WHERE t1.status=1 AND t1.id_cliente =".$this->getIdUsuario()."
    AND t2.ID NOT IN (SELECT reconexion.id_detalle_servicio FROM reconexion)" ;
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;

  }

  public function getAllReconexiones(){
    $query =
    "SELECT t1.id_detalle_servicio, t1.status,t1.id_periodo, t2.nombre, t2.descripcion, t4.nombre AS nombrePeriodo
    FROM reconexion t1
    INNER JOIN detalle_servicio t2
    ON t1.id_detalle_servicio = t2.ID
    INNER JOIN cliente_x_detalle_servicio t3
    ON t3.id_detalle_servicio = t2.ID
    INNER JOIN periodo t4
    ON t1.id_periodo = t4.id
    WHERE t3.id_cliente = ".$this->getIdUsuario();
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;
  }

  public function update(){
    $ex = false;
    $this->do_open();
    $query =  " UPDATE periodo
     SET nombre   ='".$this->getNombre()."',
     fecha_inicio  ='".$this->getfecha_inicio()."',
     fecha_fin    = '".$this->getfecha_fin()."',
     status       =".$this->getStatus()."
     WHERE id     =".$this->getId();

     $actionlog = array(
       'query' => $query,
       'table' => $this-> table,
       'action' => "update"
     );

     $ex = false;
     $res = $this->do_actionLog($actionlog);
     if ($res) {
       $ex = true;
     }
     $this->do_close();
     return $ex;
  }

}

?>
