<?php
class cls_cliente extends cls_conexion
{
    private $id;
    private $nombre;
    private $apellido;
    private $pass;
    private $cedula;
    private $status;
    private $id_roll;
    private $logueado;
    private $nickname;

  private $table = "cliente";
  function __construct()//$_id,$_nombre,$_descripcion,$_status
  {
  }
  public function getId(){return $this->id;}
  public function setId($param){$this->id = $param;}
  public function getNombre(){return $this->nombre;}
  public function setNombre($param){$this->nombre = $param;}
  public function getApellido(){return $this->apellido;}
  public function setApellido($param){$this->apellido = $param;}
  public function getPass(){return $this->pass;}
  public function setPass($param){$this->pass = $param;}
  public function getCedula(){return $this->cedula;}
  public function setCedula($param){$this->cedula = $param;}
  public function getStatus(){return $this->status;}
  public function setStatus($param){$this->status = $param;}
  public function getId_roll(){return $this->id_roll;}
  public function setId_roll($param){$this->id_roll = $param;}
  public function getLogueado(){return $this->logueado;}
  public function setLogueado($param){$this->logueado = $param;}
  public function getNickname(){return $this->nickname;}
  public function setNickname($param){$this->nickname = $param;}



  public function login(){
    $query = "
    SELECT `id`, `status`, `logueado`, `id_roll`, `nombre`, `apellido`, `pass`, `cedula`, `nickname`,`requiereCambioPass`
    FROM cliente where nickname = '".$this->getNickname()."' and pass = '".$this->getPass()."' and status = 0";
    $this->do_open();
    $res = $this->do_query($query);

    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{
      $resultSet = null;
    }
    $this->do_close();
    return $resultSet;
  }

  public function getAll(){
    $query = "SELECT `id`, `nombre`, `apellido`, `pass`, `cedula`, `status`, `id_roll`, `logueado`, `nickname` FROM cliente WHERE ".ROLL_STATUS;
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;

  }
  public function insert(){
    $ex   = false;
    $res  = null;
    $this->do_open();
    $query="
    INSERT INTO cliente (`nombre`,`apellido`, `pass`, `cedula`, `status`,`id_roll`, `logueado`, `nickname`,requiereCambioPass) VALUES(
    '".$this->getNombre()."',
    '".$this->getApellido()."',
    '".$this->getPass()."',
    '".$this->getCedula()."',
    ".$this->getStatus().",
    ".$this->getId_roll().",
    ".$this->getLogueado().",
    '".$this->getNickname()."',
    1
    );";
    $actionlog = array(
      'query' => $query,
      'table' => $this-> table,
      'action' => "insert"
    );
    $ex = false;
    $res = $this->do_actionLog($actionlog);
    if ($res) {
      $ex = true;
    }
    $this->do_close();
    return $ex;
  }
  public function getBy($_id){
    $query = "
    SELECT *
    FROM cliente
    WHERE
    id = ".$_id;
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;
  }

  public function update(){
    $ex = false;
    $query =
    " UPDATE cliente
     SET
     logueado = '".$this->getLogueado()."',
     id_roll ='".$this->getId_roll()."',
     nombre   ='".$this->getNombre()."',
     apellido  ='".$this->getApellido()."',
     pass  ='".$this->getPass()."',
     cedula  ='".$this->getCedula()."',
     nickname       ='".$this->getNickname()."',
      status =".$this->getStatus()."
     WHERE id     =".$this->getId();
    $this->do_open();
    $actionlog = array(
      'query' => $query,
      'table' => $this-> table,
      'action' => "update"
    );
    $ex = false;
    $res = $this->do_actionLog($actionlog);
    if ($res) {
      $ex = true;
    }
    $this->do_close();
    return $ex;
}
}

 ?>
