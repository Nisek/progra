<?php

class cls_periodo extends cls_conexion
{
  private $id;
  private $nombre;
  private $fecha_inicio;
  private $fecha_fin;
  private $status;
  private $table = "periodo";
  function __construct()
  {
  }
  public function getid(){return $this->id;}
  public function setid($param){$this->id=$param;}
  public function getNombre(){return $this->nombre;}
  public function setNombre($param){$this->nombre=$param;}
  public function getfecha_inicio(){return $this->fecha_inicio;}
  public function setfecha_inicio($param){$this->fecha_inicio=$param;}
  public function getfecha_fin(){return $this->fecha_fin;}
  public function setfecha_fin($param){$this->fecha_fin=$param;}
  public function getStatus(){return $this->status;}
  public function setStatus($param){$this->status=$param;}

  public function insert(){
    $ex   = false;
    $res  = null;
    $this->do_open();
    $query="
    INSERT INTO periodo (nombre,fecha_inicio,fecha_fin,status) VALUES (
    '".$this->getNombre()."',
    '".$this->getfecha_inicio()."',
    '".$this->getfecha_fin()."',
    ".$this->getStatus()."
  );";

$actionlog = array(
  'query' => $query,
  'table' => $this->table,
  'action' => "insert"
);


$ex= false;
$res = $this -> do_actionlog($actionlog);
if ($res) {
  $ex = true;
}

    $this->do_close();
    return $ex;
  }
  public function getBy($_id){
    $query = "
    SELECT *
    FROM periodo
    WHERE
    id = ".$_id;
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;
  }
  public function getAll(){
    $query = "SELECT * FROM periodo WHERE ".ROLL_STATUS;
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;

  }
  public function update(){
    $ex = false;
    $this->do_open();
    $query =  " UPDATE periodo SET status =".$this->getStatus()."
     WHERE id=".$this->getId();

     $actionlog = array(
       'query' => $query,
       'table' => $this-> table,
       'action' => "update"
     );

     $ex = false;
     $res = $this->do_actionLog($actionlog);
     if ($res) {
       $ex = true;
     }
     $this->do_close();
     return $ex;
  }

  public function getInactivadosPeriodos(){
    $query =
    "SELECT t1.id_detalle_servicio AS idNoPago
    FROM detalle_servicio_x_periodo t1
    WHERE id_periodo =".$this->getId()." AND pago = 1";

    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;
}

public function updateMulta($id){
  $ex   = false;
  $res  = null;
  $this->do_open();
  $query="
  INSERT into detalle_servicio_x_multa (id_detalle_servicio, id_multa, id_periodo, status) VALUES (
  '".$id."',
  4,
  '".$this->getId()."',
  0);";

$actionlog = array(
'query' => $query,
'table' => $this->table,
'action' => "insert"
);


$ex= false;
$res = $this -> do_actionlog($actionlog);
if ($res) {
$ex = true;
}
 }

 public function updateEstado($id){
   $ex = false;
   $this->do_open();
   $query =  " UPDATE cliente_x_detalle_servicio SET status = 1
    WHERE id_detalle_servicio=".$id;

    $actionlog = array(
      'query' => $query,
      'table' => $this-> table,
      'action' => "update"
    );

    $ex = false;
    $res = $this->do_actionLog($actionlog);
    if ($res) {
      $ex = true;
    }
    $this->do_close();
    return $ex;
  }

  public function updateDetalleServicioPeriodo($id){
    $ex = false;
    $this->do_open();
    $query =  " UPDATE detalle_servicio_x_periodo SET id_multa = 4
     WHERE id_detalle_servicio=".$id;

     $actionlog = array(
       'query' => $query,
       'table' => $this-> table,
       'action' => "update"
     );

     $ex = false;
     $res = $this->do_actionLog($actionlog);
     if ($res) {
       $ex = true;
     }
     $this->do_close();
     return $ex;
   }

}

?>
