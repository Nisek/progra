<?php

class cls_pago extends cls_conexion
{
  private $id;
  private $idParaPago;

  function __construct()
  {
  }
  public function getIdUsuario(){return $this->id;}
  public function setIdUsuario($param){$this->id=$param;}

  public function getIdParaPago(){return $this->idParaPago;}
  public function setIdParaPago($param){$this->idParaPago=$param;}


  public function insert(){
    $ex = false;
    $query="UPDATE detalle_servicio_x_periodo set pago = 0, id_multa= 0 where id = ".$this->getIdParaPago();
    $this->do_open();
    $actionlog = array(
      'query' => $query,
      'table' => 'detalle_servicio_x_periodo',
      'action' => "update"
    );
    $ex = false;
    $res = $this->do_actionLog($actionlog);
    if ($res) {
      $ex = true;
    }
    $this->do_close();
    return $ex;
}

  public function getBy($_id){
    $query = "
    SELECT *
    FROM periodo
    WHERE
    id = ".$_id;
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;
  }

 public function getAll(){
   $query =
   "SELECT t1.id AS idDelCxDS, t1.id_detalle_servicio, t2.nombre as nombreDS,
   t2.descripcion, t3.id_periodo, t4.nombre AS nombrePeriodo,t3.pago, t3.id AS IdDSxPParaPago, t2.precio, t5.total
   FROM endesa.cliente_x_detalle_servicio t1
   INNER JOIN endesa.detalle_servicio t2
   ON t1.id_detalle_servicio = t2.ID
   INNER JOIN endesa.detalle_servicio_x_periodo t3
   ON t3.id_detalle_servicio = t2.ID
   INNER JOIN endesa.periodo t4
   ON t3.id_periodo = t4.id
   INNER JOIN multa t5
   ON t5.id = t3.id_multa
   WHERE t1.status=1 AND t3.pago =1 and t1.id_cliente =".$this->getIdUsuario();
   $this->do_open();
   $res = $this->do_query($query);
   if ($res->num_rows > 0) {
     while ($row = $res->fetch_object()) {//para parsear el resultado del select
        $resultSet[]=$row;
     }
   }else{$resultSet = null;}
   $this->do_close();
   return $resultSet;

 }

  public function update(){
    $ex = false;
    $this->do_open();
    $query =  " UPDATE periodo
     SET nombre   ='".$this->getNombre()."',
     fecha_inicio  ='".$this->getfecha_inicio()."',
     fecha_fin    = '".$this->getfecha_fin()."',
     status       =".$this->getStatus()."
     WHERE id     =".$this->getId();

     $actionlog = array(
       'query' => $query,
       'table' => $this-> table,
       'action' => "update"
     );

     $ex = false;
     $res = $this->do_actionLog($actionlog);
     if ($res) {
       $ex = true;
     }
     $this->do_close();
     return $ex;
  }

}

?>
