  <?php
/**
 *
 */
class cls_detalle extends cls_conexion
{
  private $ID,$id_servicio,$nombre,$descripcion,$precio,$status,$idUsuario;
  private $table = "detalle_servicio";
  function __construct()
  {
  }
  public function getID(){return $this->ID;}
  public function setID($param){$this->ID = $param;}

  public function getIdUsuario(){return $this->idUsuario;}
  public function setIdUsuario($param){$this->idUsuario = $param;}

  public function getId_servicio(){return $this->id_servicio;}
  public function setId_servicio($param){$this->id_servicio = $param;}
  public function getNombre(){return $this->nombre;}
  public function setNombre($param){$this->nombre = $param;}

  public function getDescripcion(){return $this->descripcion;}
  public function setDescripcion($param){$this->descripcion = $param;}
  public function getPrecio(){return $this->precio;}
  public function setPrecio($param){$this->precio = $param;}
  public function getStatus(){return $this->status;}
  public function setStatus($param){

    if (!isset($param) || empty($param) || $param = '' || is_null($param)) {
        $this->status = 0;
    }else{
      $this->status = $param;
    }


  }

public function getAll(){
  $query = "SELECT * FROM $this->table WHERE ".ROLL_STATUS;
  $this->do_open();
  $res = $this->do_query($query);
  if ($res->num_rows > 0) {
    while ($row = $res->fetch_object()) {//para parsear el resultado del select
       $resultSet[]=$row;
    }
  }else{$resultSet = null;}
  $this->do_close();
  return $resultSet;
}

public function getAllAsignadosAPeriodos(){
  $query =
  "SELECT t1.* FROM detalle_servicio t1
  INNER JOIN detalle_servicio_x_periodo t2
  ON t1.ID = t2.id_detalle_servicio
  WHERE t1.`status`= 0";
  $this->do_open();
  $res = $this->do_query($query);
  if ($res->num_rows > 0) {
    while ($row = $res->fetch_object()) {//para parsear el resultado del select
       $resultSet[]=$row;
    }
  }else{$resultSet = null;}
  $this->do_close();
  return $resultSet;
}


public function getAllMisServicios(){
  $query =
  "SELECT t3.nombre AS nombreServicio, t2.nombre AS nombreDetalle, t2.descripcion,
  t2.precio, t1.status, t1.id, t5.nombre AS nombreMulta, t5.total AS montoMulta, t5.descripcion AS descripcionMulta
  FROM endesa.cliente_x_detalle_servicio t1
  INNER JOIN endesa.detalle_servicio t2
  ON t1.id_detalle_servicio = t2.ID
  INNER JOIN endesa.servicio t3
  ON t2.id_servicio = t3.id
  INNER JOIN detalle_servicio_x_multa t4
  ON t2.ID = t4.id_detalle_servicio
  INNER JOIN multa t5
  ON t4.id_multa = t5.id
  WHERE  t1.status=1 AND t1.id_cliente =".$this->getIdUsuario();
  $this->do_open();
  $res = $this->do_query($query);

  if ($res->num_rows > 0) {
    while ($row = $res->fetch_object()) {//para parsear el resultado del select
       $resultSet[]=$row;
    }
  }else{$resultSet = null;}
  $this->do_close();
  return $resultSet;
}

public function getAllMisServiciosActivos(){
  $query =
  "SELECT t3.nombre AS nombreServicio, t2.nombre AS nombreDetalle, t2.descripcion,
  t2.precio, t1.status, t1.id
  FROM endesa.cliente_x_detalle_servicio t1
  INNER JOIN endesa.detalle_servicio t2
  ON t1.id_detalle_servicio = t2.ID
  INNER JOIN endesa.servicio t3
  ON t2.id_servicio = t3.id
  WHERE t1.id_cliente =".$this->getIdUsuario();
  $this->do_open();
  $res = $this->do_query($query);

  if ($res->num_rows > 0) {
    while ($row = $res->fetch_object()) {//para parsear el resultado del select
       $resultSet[]=$row;
    }
  }else{$resultSet = null;}
  $this->do_close();
  return $resultSet;
}

public function insert(){
  $ex   = false;
  $res  = null;
  $this->do_open();
  $query="
  INSERT INTO detalle_servicio (`id_servicio`,`nombre`, `descripcion`, `precio`, `status`) VALUES(
  ".$this->getId_servicio().",
  '".$this->getNombre()."',
  '".$this->getDescripcion()."',
  ".$this->getPrecio().",
  ".$this->getStatus()."
  );";
  $actionlog = array(
    'query' => $query,
    'table' => $this->table,
    'action' => "insert"
  );
  $ex = false;
  $res = $this->do_actionLog($actionlog);
  if ($res) {
    $ex = true;
  }
  $this->do_close();
  return $ex;
}

public function getBy($_ID){
  $query = "
  SELECT `ID`, `id_servicio`, `nombre`, `descripcion`, `precio`, `status`
  FROM detalle_servicio
  WHERE
  ID = ".$_ID;
  $this->do_open();
  $res = $this->do_query($query);
  if ($res->num_rows > 0) {
    while ($row = $res->fetch_object()) {//para parsear el resultado del select
       $resultSet[]=$row;
    }
  }else{$resultSet = null;}
  $this->do_close();
  return $resultSet;
}

public function update(){
  $ex = false;
  $query =
  " UPDATE detalle_servicio
   SET id_servicio =".$this->getId_servicio().",
   nombre = '".$this->getNombre()."',
   descripcion ='".$this->getDescripcion()."',
   precio   =".$this->getPrecio().",
   status  =".$this->getStatus()."
   WHERE ID =".$this->getID();
  $this->do_open();
  $actionlog = array(
    'query' => $query,
    'table' => $this-> table,
    'action' => "update"
  );
  $ex = false;
  $res = $this->do_actionLog($actionlog);
  if ($res) {
    $ex = true;
  }
  $this->do_close();
  return $ex;
}

}
 ?>
