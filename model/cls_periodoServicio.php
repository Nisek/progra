<?php

class cls_periodoServicio extends cls_conexion
{
  private $id;
  private $id_periodo;
  private $id_detalle_servicio;
  private $total;
  private $pago;
  private $id_multa;
  private $status;
  private $table = "periodoServicio";
  function __construct()
  {
  }
  public function getid(){return $this->id;}
  public function setid($param){$this->id = $param;}
  public function getid_periodo(){return $this->id_periodo;}
  public function setid_periodo($param){$this->id_periodo = $param;}
  public function getid_detalle_servicio(){return $this->id_detalle_servicio;}
  public function setid_detalle_servicio($param){$this->id_detalle_servicio = $param;}
  public function gettotal(){return $this->total;}
  public function settotal($param){$this->total = $param;}
  public function getstatus(){return $this->status;}
  public function setstatus($param){$this->status = $param;}

//----------------------------------------METODO INSERT
  public function insert(){
    $ex   = false;
    $res  = null;
    $this->do_open();
    $query="
    INSERT INTO detalle_servicio_x_periodo (status,id_periodo,id_detalle_servicio,total,pago,id_multa)
    VALUES
    (
    '".$this->getStatus()."',
    '".$this->getid_periodo()."',
    '".$this->getid_detalle_servicio()."',
    '".$this->gettotal()."',
    '1',
    '0'
    );";

    $actionlog = array(
      'query' => $query,
      'table' => $this->table,
      'action' => "insert"
    );


    $ex= false;
    $res = $this -> do_actionlog($actionlog);
    if ($res) {
      $ex = true;
    }

      $this->do_close();
      return $ex;
  }
//----------------------------------------
  public function getBy($_id){
    $query = "
    SELECT *
    FROM detalle_servicio_x_periodo
    WHERE
    id = ".$_id;
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;
  }
  //----------------------------------------METODO OBTENER
  public function getAll(){
    $query = "SELECT * FROM detalle_servicio WHERE ".ROLL_STATUS;
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;

  }

  public function getAllDatos(){
    $query =
    "SELECT t3.status,t3.nombre AS nombrePeriodo, t2.nombre, t2.descripcion,
    t1.total, t1.pago, t1.id_multa,t1.id
    FROM endesa.detalle_servicio_x_periodo t1
    INNER JOIN endesa.detalle_servicio t2
    ON t1.id_detalle_servicio = t2.ID
    INNER JOIN endesa.periodo t3
    ON t1.id_periodo = t3.id";
    $this->do_open();
    $res = $this->do_query($query);
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    $this->do_close();
    return $resultSet;

  }

  //----------------------------------------METODO UPDATE
  public function update(){
    $ex = false;
    $this->do_open();
    $query =  " UPDATE detalle_servicio_x_periodo
     SET
     id_periodo  ='".$this->getid_periodo()."',
     status       =".$this->getid_multa()."
     WHERE id     =".$this->getId();

     $actionlog = array(
       'query' => $query,
       'table' => $this-> table,
       'action' => "update"
     );

     $ex = false;
     $res = $this->do_actionLog($actionlog);
     if ($res) {
       $ex = true;
     }
     $this->do_close();
     return $ex;
  }

}

?>
