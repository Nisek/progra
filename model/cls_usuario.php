<?php
/**
 *
 */
class cls_usuario extends cls_conexion
{
  private $id,$nombre,$apellido,$status,$logueado,$id_roll,$pass,$cedula,$nickname;
  private $table = "usuario";
  function __construct()
  {
  }
  public function getId(){return $this->id;}
  public function setId($param){$this->id = $param;}
  public function getStatus(){return $this->status;}
  public function setStatus($param){$this->status = $param;}
  public function getLogueado(){return $this->logueado;}
  public function setLogueado($param){$this->logueado = $param;}
  public function getId_roll(){return $this->id_roll;}
  public function setId_roll($param){$this->id_roll = $param;}
  public function getNombre(){return $this->nombre;}
  public function setNombre($param){$this->nombre = $param;}
  public function getApellido(){return $this->apellido;}
  public function setApellido($param){$this->apellido = $param;}
  public function getPass(){return $this->pass;}
  public function setPass($param){$this->pass = $param;}
  public function getCedula(){return $this->cedula;}
  public function setCedula($param){$this->cedula = $param;}
  public function getNickname(){return $this->nickname;}
  public function setNickname($param){$this->nickname = $param;}


  public function login(){
    $query = "
    SELECT `id`, `status`, `logueado`, `id_roll`, `nombre`, `apellido`, `pass`, `cedula`, `nickname`,`requiereCambioPass`
    FROM usuario where nickname = '".$this->getNickname()."' and pass = '".$this->getPass()."' and status = 0";
    $this->do_open();
    $res = $this->do_query($query);

    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {//para parsear el resultado del select
         $resultSet[]=$row;
      }
    }else{
      $resultSet = null;
    }
    $this->do_close();
    return $resultSet;
  }


public function getAll(){
  $query = "SELECT * FROM $this->table";
  $this->do_open();
  $res = $this->do_query($query);
  if ($res->num_rows > 0) {
    while ($row = $res->fetch_object()) {//para parsear el resultado del select
       $resultSet[]=$row;
    }
  }else{$resultSet = null;}
  $this->do_close();
  return $resultSet;

}

public function insert(){
  $ex   = false;
  $res  = null;
  $this->do_open();
  $query="
  INSERT INTO usuario (`status`,`logueado`, `id_roll`, `nombre`, `apellido`,`pass`, `cedula`, `nickname`,`requiereCambioPass`) VALUES(
  ".$this->getStatus().",
  ".$this->getLogueado().",
  ".$this->getId_roll().",
  '".$this->getNombre()."',
  '".$this->getApellido()."',
  '".$this->getPass()."',
  '".$this->getCedula()."',
  '".$this->getNickname()."',
  1
  );";
  $actionlog = array(
    'query' => $query,
    'table' => $this->table,
    'action' => "insert"
  );
  $ex = false;
  $res = $this->do_actionLog($actionlog);
  if ($res) {
    $ex = true;
  }
  $this->do_close();
  return $ex;
}

public function getBy($_id){
  $query = "
  SELECT *
  FROM usuario
  WHERE
  id = ".$_id;
  $this->do_open();
  $res = $this->do_query($query);
  if ($res->num_rows > 0) {
    while ($row = $res->fetch_object()) {//para parsear el resultado del select
       $resultSet[]=$row;
    }
  }else{$resultSet = null;}
  $this->do_close();
  return $resultSet;
}

public function update(){
  $ex = false;
  $query =
  " UPDATE usuario
   SET status ='".$this->getStatus()."',
   logueado = '".$this->getLogueado()."',
   id_roll ='".$this->getId_roll()."',
   nombre   ='".$this->getNombre()."',
   apellido  ='".$this->getApellido()."',
   pass  ='".$this->getPass()."',
   cedula  ='".$this->getCedula()."',
   nickname       ='".$this->getNickname()."'
   WHERE id     =".$this->getId();
  $this->do_open();
  $actionlog = array(
    'query' => $query,
    'table' => $this-> table,
    'action' => "update"
  );
  $ex = false;
  $res = $this->do_actionLog($actionlog);
  if ($res) {
    $ex = true;
  }
  $this->do_close();
  return $ex;
}

}
 ?>
