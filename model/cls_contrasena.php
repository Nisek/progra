<?php
class cls_contrasena extends cls_conexion
{
    private $id;
    private $pass;
    private $id_roll;
    private $cedula;

  function __construct()
  {
  }

  public function getId(){return $this->id;}
  public function setId($param){$this->id = $param;}
  public function getPass(){return $this->pass;}
  public function setPass($param){$this->pass = $param;}
  public function getId_roll(){return $this->id_roll;}
  public function setId_roll($param){$this->id_roll = $param;}
  public function getCedula(){return $this->cedula;}
  public function setCedula($param){$this->cedula = $param;}

  public function update(){
    $table = $this->obtenerLaTabla();
    $ex = false;
    $query =
    " UPDATE $table
     SET
     pass  ='".$this->getPass()."',
     requiereCambioPass = 0
     WHERE id  =".$this->getId();
    $this->do_open();
    $actionlog = array(
      'query' => $query,
      'table' => $this-> table,
      'action' => "update"
    );
    $ex = false;
    $res = $this->do_actionLog($actionlog);
    if ($res) {
      $ex = true;
    }
    $this->do_close();
    return $ex;
  }

  public function obtenerLaTabla(){
    $tablaDelUpdate = null;
    $roll= $this->getId_roll();

    if ($roll == 3) {
      $tablaDelUpdate = "cliente";
    }
    else {
      $tablaDelUpdate = "usuario";
    }
    return $tablaDelUpdate;
  }

  public function buscarUsuario(){
    $query = "
    SELECT *
    FROM usuario
    WHERE
    cedula = ".$this->getCedula();
    $this->do_open();
    $res = $this->do_query($query);
      if ($res) {
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    }
    else {
      $resultSet = null;
    }
    $this->do_close();
    return $resultSet;
  }

  public function buscarCliente(){
    $query = "
    SELECT *
    FROM cliente
    WHERE
    cedula = ".$this->getCedula();
    $this->do_open();
    $res = $this->do_query($query);
      if ($res) {
    if ($res->num_rows > 0) {
      while ($row = $res->fetch_object()) {
         $resultSet[]=$row;
      }
    }else{$resultSet = null;}
    }
    else {
      $resultSet = null;
    }
    $this->do_close();
    return $resultSet;
  }

  public function updateUsuario(){
    $nuevoPass =$this->getPass();
    $ex   = false;
    $res  = null;
    $this->do_open();
    $_SESSION['usuario']['id']=1; //Usuario guest
    $query="UPDATE usuario set pass = '$nuevoPass', requiereCambioPass= 1 where id = ".$this->getId();
    $actionlog = array(
      'query' => $query,
      'table' => "usuario",
      'action' => "update"
    );
    $ex = false;
    $res = $this->do_actionLog($actionlog);
    if ($res) {
      $ex = true;
    }
    $this->do_close();
    return $ex;
  }

  public function updateCliente(){
    $nuevoPass =$this->getPass();
    $ex   = false;
    $res  = null;
    $this->do_open();
    $_SESSION['usuario']['id']=1; //Usuario guest
    $query="UPDATE cliente set pass = '$nuevoPass', requiereCambioPass= 1 where id = ".$this->getId();
    $actionlog = array(
      'query' => $query,
      'table' => "cliente",
      'action' => "update"
    );
    $ex = false;
    $res = $this->do_actionLog($actionlog);
    if ($res) {
      $ex = true;
    }
    $this->do_close();
    return $ex;
  }

}
?>
